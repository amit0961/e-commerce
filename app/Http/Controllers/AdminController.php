<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class AdminController extends Controller
{
    public function login(Request $request)
    {

        if ($request->isMethod('post')){
            $data =$request->input();
            if (Auth::attempt(['email'=>$data['email'],'password'=>$data['password'], 'admin'=>'1'])){
//                echo 'Success', die;
                return redirect('/admin/dashboard');
            }else{
//                echo 'Failed', die;
                return redirect('/admin')->with('error', 'Invalid Username or Password');
            }
        }
        return view('backend.login');
    }

    public function dashboard(){
        return view('backend.dashboard');
    }
    public function settings(){
            return view('backend.settings');
        }


    //logout>>>>>>>>>>
    public function logout(){
        Session::flush();
        return redirect('/admin')->with('success', 'Logged out Successfully');
    }


    public function checkPassword(Request $request){
    $data = $request->all();
    $current_password = $data['current_pwd'];
    $check_password = User::where(['admin'=>'1'])->first();
    if (Hash::check($current_password,$check_password->password)){
    echo "true"; die;
    }else{
    echo "false"; die;
    }
    }

//    public function checkPassword(Request $request){
//        if ($request->isMethod('post')){
//            $data = $request->all();
//            $check_password = User::where(['email'=>Auth::user()->email])->first();
//            $current_password = $data['current_pwd'];
//            if (Hash::check($current_password,$check_password->password)){
//                User::where('id','1');
//                return redirect('/admin/settings')->with('success', 'Password Updated Successfully');
//            }else{
//                return redirect('/admin/settings')->with('error', 'Opps..! Incorrect Password');
//            }
//        }
//    }



    public function update(Request $request)
    {
        if ($request->isMethod('post')){
            $data = $request->all();
            $check_password = User::where(['email'=>Auth::user()->email])->first();
            $current_password = $data['current_pwd'];
            if (Hash::check($current_password,$check_password->password)){
                $password = bcrypt($data['new_pwd']);
                User::where('id','1')->update(['password'=>$password]);
                return redirect('/admin/settings')->with('success', 'Password Updated Successfully');
            }else{
                return redirect('/admin/settings')->with('error', 'Opps..! Incorrect Current Password');
            }
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
