<?php

namespace App\Http\Controllers;

use App\Banner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;

class BannerController extends Controller
{
    public function addBanner(Request $request){
        if ($request->isMethod('post')) {
            $data = $request->all();
            $banners = new Banner;
            $banners->title = $data['title'];
            $banners->link = $data['link'];
            if (empty($data['status'])){
                $status = 0;
            }else{
                $status=1;
            }
            $banners->status = $status;
            //Upload Image Section
            if ($request->hasFile('image')){
                $img = Input::file('image');
                if ($img->isValid()){
                    $extension = $img->getClientOriginalExtension();
                    $filename = rand(111,99999).'.'.$extension;
                    $banners_path ='images/products/banner/'.$filename;
                    //Resize Image
                    Image::make($img)->save($banners_path);
                    //store image in products table
                    $banners->image =$filename;
                }
            }
            $banners->save();


            return redirect()->back()->with('success','Banner has been Added Successfully');
        }
        return view('backend.banners.addBanner');
    }

    public function viewBanner(){
        $banners =Banner::get();
        return view('backend.banners.viewBanner',compact('banners'));
    }
    public function editBanner(Request $request ,$id=null){
        if ($request->isMethod('post')) {
            $data = $request->all();
            if (empty($data['status'])) {
                $status = 0;
            } else {
                $status = 1;
            }
            //Upload Image Section
            if ($request->hasFile('image')){
                $img = Input::file('image');
                if ($img->isValid()){
                    $extension = $img->getClientOriginalExtension();
                    $fileName = rand(111,99999).'.'.$extension;
                    $banner_path ='images/products/banner/'.$fileName;
                    //Resize Image
                    Image::make($img)->encode('jpg')->save($banner_path);
                }else{
                    $fileName = $data['current_image'];
                }
            }
            Banner::where(['id'=>$id])->update([
                'title'=>$data['title'],
                'link'=>$data['link'],
                'image'=>$fileName,
                'status'=>$status
            ]);
            return redirect('/admin/view-banner')->with('success','Banner Updated Successfully...!!');

        }
        $banners = Banner::where('id',$id)->first();
        return view('backend.banners.editBanner',compact('banners'));
    }
    public function deleteBanner(Request $request, $id=null)
    {
        if (!empty($id)){
            Banner::where(['id'=>$id])->forceDelete();
            return redirect('/admin/view-banner')->with('success','Banner Deleted Successfully......!!');
        }
    }
}
