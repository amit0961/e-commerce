<?php

namespace App\Http\Controllers;

use App\Category;
use DemeterChain\C;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->isMethod('post')){
            $data =$request->all();
            if (empty($data['status'])){
                $status = 0;
            }else{
                $status=1;
            }
            $categories = new Category;
            $categories->name = $data['category_name'];
            $categories->parent_id = $data['parent_id'];
            $categories->description = $data['description'];
            $categories->url = $data['url'];
            $categories->status =$status;
            $categories->save();
            return redirect('/admin/view-category')->with('success','Category Added Successfully');
        }
        $levels =  Category::where(['parent_id'=>0])->get();
        return view('backend.categories.addCategories',compact('levels'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $categories = Category::with('sub_category')->get();
        return view('backend.categories.viewCategories', compact('categories'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request , $id = null)
    {
        if ($request->isMethod('post')){
        $data = $request->all();
            if (empty($data['status'])){
                $status = 0;
            }else{
                $status=1;
            }
//        echo "<pre>"; print_r($data); die ;
            Category::where(['id'=>$id])->update(['name'=>$data['category_name'], 'description'=>$data['description'], 'url'=>$data['url'],'status'=>$status]);
            return redirect('/admin/view-category')->with('success','Category Updated Successfully');
    }
        $categories = Category::where(['id'=>$id])->first();
        $levels =  Category::where(['parent_id'=>0])->get();
        return view('backend.categories.editCategories', compact('categories','levels'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request , $id=null)
    {
        if (!empty($id)){
            Category::where(['id'=>$id])->delete();
            return redirect('/admin/view-category')->with('success','Category Deleted Successfully');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //
    }
}
