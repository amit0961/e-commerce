<?php

namespace App\Http\Controllers;

use App\Coupon;
use Illuminate\Http\Request;

class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addCoupon(Request $request)
    {
        if ($request->isMethod('post')){
            $data = $request->all();
            if (empty($data['status'])){
                $status = 0;
            }else{
                $status=1;
            }
            $coupons= new Coupon;
            $coupons->coupon_code = $data['coupon_code'];
            $coupons->amount = $data['amount'];
            $coupons->amount_type = $data['amount_type'];
            $coupons->expiry_date = $data['expiry_date'];
            $coupons->status = $status;
            $coupons->save();
            return redirect('/admin/view-coupon')->with('success','Coupon has been Added Successfully');
        }
        return view('backend.coupons.addCoupon');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewCoupons()
    {
        $coupons = Coupon::get();
        return view('backend.coupons.viewCoupon', compact('coupons'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function editCoupon(Request $request,$id=null)
    {
        if ($request->isMethod('post')) {
            $data = $request->all();
            if (empty($data['status'])){
                $status = 0;
            }else{
                $status=1;
            }

            Coupon::where(['id'=>$id])->update([
                'coupon_code' => $data['coupon_code'],
                'amount' => $data['amount'],
                'amount_type' => $data['amount_type'],
                'expiry_date' => $data['expiry_date'],
                'status' => $status,
            ]);
            return redirect('/admin/view-coupon')->with('success','Coupon Updated Successfully');

        }
        $coupons = Coupon::where(['id'=>$id])->first();
        return view('backend.coupons.editCoupon',compact('coupons'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteCoupon($id)
    {
        if (!empty($id)){
            Coupon::where(['id'=>$id])->delete();
            return redirect('/admin/view-coupon')->with('success','Coupon Deleted Successfully');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
