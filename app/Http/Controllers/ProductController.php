<?php

namespace App\Http\Controllers;

use App\Category;
use App\Country;
use App\Coupon;
use App\Deliveryaddress;
use App\Order;
use App\OrdersProduct;
use App\Product;
use App\ProductAttribute;
use App\ProductImage;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use phpDocumentor\Reflection\DocBlock\Tag;
use Symfony\Component\HttpKernel\Profiler\Profile;

class ProductController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->isMethod('post')){
            $data =$request->all();
            if (empty($data['category_id'])){
                return redirect()->back()->with('error','Under Category is Missing');
            }

            $products = new Product;
            $products->category_id = $data['category_id'];
            $products->product_name = $data['product_name'];
            $products->product_code = $data['product_code'];
            $products->product_color = $data['product_color'];
            $products->description = $data['description'];
            $products->care = $data['care'];
            $products->price = $data['price'];
            //Upload Image Section
            if ($request->hasFile('image')){
                $img = Input::file('image');
                if ($img->isValid()){
                    $extension = $img->getClientOriginalExtension();
                    $filename = rand(111,99999).'.'.$extension;
                    $large_image_path ='images/products/large/'.$filename;
                    $medium_image_path ='images/products/medium/'.$filename;
                    $small_image_path ='images/products/small/'.$filename;
                    //Resize Image
                    Image::make($img)->encode('jpg')->save($large_image_path);
                    Image::make($img)->encode('jpg')->resize(600,600)->save($medium_image_path);
                    Image::make($img)->encode('jpg')->resize(300,300)->save($small_image_path);
                    //store image in products table
                    $products->image =$filename;
                }
            }
            if (empty($data['status'])){
                $status = 0;
            }else{
                $status=1;
            }
            $products->status = $status;
            $products->save();
            return redirect('/admin/view-product')->with('success','Product has been Added Successfully');
        }
        $categories =  Category::where(['parent_id'=>0])->get();
        $categories_dropdown = "<option selected disabled>Select<option>";
        foreach ($categories as $category){
            $categories_dropdown .="<option value='".$category->id."'>".$category->name."<option>";
            $subcategories =  Category::where(['parent_id'=>$category->id])->get();
            foreach ($subcategories as $subcategory){
                $categories_dropdown .="<option value='".$subcategory->id."'> &nbsp;--&nbsp; ".$subcategory->name."<option>";
            }
        }

        return view('backend.products.addProducts',compact('categories_dropdown'));
    }


    public function store(Request $request)
    {
        //
    }


    public function show()
    {
        $products =Product::get();
        $products = json_decode(json_encode($products));
        foreach ($products as $key=>$val){
            $category_name = Category::where(['id'=> $val->category_id])->first();
            $products[$key]->category_name = $category_name['name'];
        }

        return view('backend.products.viewProducts',compact('products'));
    }

    public function edit(Request $request,$id=null)
    {
        if ($request->isMethod('post')) {
            $data = $request->all();
            if (empty($data['status'])){
                $status = 0;
            }else{
                $status=1;
            }
            //Upload Image Section
            if ($request->hasFile('image')){
                $img = Input::file('image');
                if ($img->isValid()){
                    $extension = $img->getClientOriginalExtension();
                    $fileName = rand(111,99999).'.'.$extension;
                    $large_image_path ='images/products/large/'.$fileName;
                    $medium_image_path ='images/products/medium/'.$fileName;
                    $small_image_path ='images/products/small/'.$fileName;
                    //Resize Image
                    Image::make($img)->encode('jpg')->save($large_image_path);
                    Image::make($img)->encode('jpg')->resize(600,600)->save($medium_image_path);
                    Image::make($img)->encode('jpg')->resize(300,300)->save($small_image_path);
                }else{
                    $fileName = $data['current_image'];
                }
            }
            Product::where(['id'=>$id])->update([
                'category_id'=>$data['category_id'],
                'product_name'=>$data['product_name'],
                'product_code'=>$data['product_code'],
                'product_color'=>$data['product_color'],
                'description'=>$data['description'],
                'care'=>$data['care'],
                'price'=>$data['price'],
                'image'=>$fileName,
                'status'=>$status
            ]);
            return redirect('/admin/view-product')->with('success','Product Updated Successfully');
        }
        $products = Product::where(['id'=>$id])->first();
        //dropdown starts here
        $categories =  Category::where(['parent_id'=>0])->get();
        $categories_dropdown = "<option selected disabled>Select<option>";
        foreach ($categories as $category){
            if ($category->id==$products->category_id){
                $selected = "selected";
            }else{
                $selected = "";
            }
            $categories_dropdown .="<option value='".$category->id."' ".$selected.">".$category->name."<option>";
            $subcategories =  Category::where(['parent_id'=>$category->id])->get();
            foreach ($subcategories as $subcategory){
                if ($subcategory->id==$products->category_id){
                    $selected = "selected";
                }else{
                    $selected = "";
                }
                $categories_dropdown .="<option value='".$subcategory->id."' ".$selected."> &nbsp;--&nbsp; ".$subcategory->name."<option>";
            }
        }
        //dropdown end here
        return view('backend.products.editProducts',compact('products','categories_dropdown'));

    }

    public function delete(Request $request, $id=null)
    {
        if (!empty($id)){
            Product::where(['id'=>$id])->delete();
            return redirect('/admin/view-product')->with('success','Product Deleted Successfully');
        }
    }

    public function deleteImage( $id=null)
    {
        //delete image from folder
        $productImage =Product::where(['id'=>$id])->first();
        $largeImagePath ='images/products/large/';
        $mediumImagePath ='images/products/medium/';
        $smallImagePath ='images/products/small/';
        if (file_exists($largeImagePath.$productImage->image)){
            unlink($largeImagePath.$productImage->image);
        }
        if (file_exists($mediumImagePath.$productImage->image)){
            unlink($mediumImagePath.$productImage->image);
        }
        if (file_exists($smallImagePath.$productImage->image)){
            unlink($smallImagePath.$productImage->image);
        }
        //deleteimagefromFolder
            Product::where(['id'=>$id])->update(['image'=>'']);
            return redirect()->back()->with('success','Product Image has been Deleted Successfully');
    }

    public function addAttribute(Request $request, $id=null)
    {
        $products = Product::with('productattribute')->where(['id'=>$id])->first();
        if ($request->isMethod('post')) {
            $data = $request->all();
            foreach ($data['sku'] as $key=>$val){
                if (!empty($val)){
                    //sku check
                    $attrCountSKU =ProductAttribute::where('sku',$val)->count();
                    if($attrCountSKU>0){
                        return redirect('admin/add-attribute/'.$id)->with('error','Opps!!! Product SKU already exist.');
                    }
                    //duplicate size prevent
                    $attrCountSize =ProductAttribute::where(['product_id'=>$id,'size'=>$data['size'][$key]])->count();
                    if($attrCountSize>0){
                        return redirect('admin/add-attribute/'.$id)->with('error', 'Opps!!! "'.$data['size'][$key].'" Size of this product already exist.');
                    }
                    //duplicateSize
                    $attribute =new ProductAttribute;
                    $attribute->product_id = $id;
                    $attribute->sku=$val;
                    $attribute->size = $data['size'][$key];
                    $attribute->price = $data['price'][$key];
                    $attribute->stock = $data['stock'][$key];
                    $attribute->save();
                }
            }
            return redirect('admin/add-attribute/'.$id)->with('success','Product Attribute has been Added Successfully');

        }

//        $products = json_decode(json_encode($products));
//        echo "<pre>";  print_r($products); die;
        return view('backend.products.addAttribute', compact('products'));

    }

    public function editAttribute(Request $request, $id=null){
        if ($request->isMethod('post')){
            $data = $request->all();
            foreach ($data['idAttr'] as $key=>$attr) {
                ProductAttribute::where(['id'=>$data['idAttr'][$key]])->update(['price'=>$data['price'][$key],'stock'=>$data['stock'][$key]]);
            }
        }
        return redirect()->back()->with('success' ,'Product Attribute has been Updated Successfully');

    }

    public function addImages(Request $request, $id=null)
    {
        $products = Product::with('productattribute')->where(['id'=>$id])->first();
        if ($request->isMethod('post')) {
            $data = $request->all();
            if ($request->hasFile('image')){
                $files = $request->file('image');
                foreach ($files as $file){
                    $image = new ProductImage;
                    $extension = $file->getClientOriginalExtension();
                    $fileName = rand(111,99999).'.'.$extension;
                    $large_image_path ='images/products/large/'.$fileName;
                    $medium_image_path ='images/products/medium/'.$fileName;
                    $small_image_path ='images/products/small/'.$fileName;
                    //Resize Image
                    Image::make($file)->encode('jpg')->save($large_image_path);
                    Image::make($file)->encode('jpg')->resize(600,600)->save($medium_image_path);
                    Image::make($file)->encode('jpg')->resize(300,300)->save($small_image_path);
                    $image->image =$fileName;
                    $image->product_id =$data['product_id'];
                    $image->save();
                }
            }
            return redirect('admin/add-images/'.$id)->with('success','Product Image Added Successfully');
        }

        $productImages = ProductImage::where(['product_id'=>$id])->get();

        return view('backend.products.addImages', compact('products','productImages'));
    }
    public function deleteAttribute(Request $request, $id=null)
    {
        if (!empty($id)) {
            ProductAttribute::where(['id' => $id])->forceDelete();
            return redirect()->back()->with('success', 'Product Attribute has been Deleted Successfully');
        }

    }
    public function deleteProImage(Request $request, $id=null)
    {
        if (!empty($id)) {
            ProductImage::where(['id' => $id])->forceDelete();
            return redirect()->back()->with('success', 'Product Image has been Deleted Successfully');
        }

    }
    public function products($url=null){
        //show error page
        $countCategory = Category::where(['url'=>$url,'status'=>1])->count();
        if ($countCategory==0){
            abort(404);
        }

        //All Categories And SubCategories
        $categories =Category::with('categories')->where(['parent_id'=>0])->get();
        $categoryDetails =  Category::where(['url'=>$url])->first();
//        if($categoryDetails->perent_id==0){
//            $subCategories =  Category::where(['parent_id'=>$categoryDetails->id])->get();
////            foreach($subCategories as $subCategory){
////                $category_ids[] = $subCategory->id;
////            }
////            echo $mainCategory_ids; die;
//            $products =  Product::whereIn(['category_id'=>$subCategories->id])->where('status','1')->get();
////            $products = json_decode(json_encode($products));
////            echo "<pre>"; print_r($products);
//        }else{
//            $products =  Product::where(['category_id'=>$categoryDetails->id])->where('status','1')->get();
//        }

        $products =  Product::where(['category_id'=>$categoryDetails->id])->where('status','1')->get();
//        $subCategories =  Category::where(['parent_id'=>$categoryDetails->id])->get();
        return view('frontend.productList',compact('categories','categoryDetails','products'));
    }

    public function productDetails($id=null){
        $countProduct= Product::where(['id'=>$id,'status'=>1])->count();
        if ($countProduct==0){
            abort(404);
        }
        $products = Product::with('productattribute')->where('id',$id)->first();
        $products=json_decode(json_encode($products));
//        echo "<pre>"; print_r($products); die;
        //
        $relatedProducts = Product::where('id','!=',$id)->where(['category_id'=>$products->category_id])->get();
//        foreach ($relatedProducts->chunk(3) as $chunk){
//            foreach ($chunk as $item){
//                echo $item; echo "<br>";
//            }
//            echo "<br> <br> <br>";
//        }
//        die;



        $categories =Category::with('categories')->where(['parent_id'=>0])->get();
        //get product Alternate Images
        $proAltImages = ProductImage::where('product_id',$id)->get();
        $totalStock = ProductAttribute::where('product_id',$id)->sum('stock');
        return view('frontend.productDetails',compact('products','categories','proAltImages','totalStock','relatedProducts'));
    }

    public function getproductPrice(Request $request){
        $data = $request->all();
//        echo "<pre>"; print_r($data); die;
        $proArr = explode("-", $data['idSize']);
        $proArr = ProductAttribute::where(['product_id'=>$proArr[0],'size'=>$proArr[1]])->first();
        echo $proArr->price;
        echo "#";
        echo $proArr->stock;
    }

    public function addToCart(Request $request){
        Session::forget('CouponAmount');
        Session::forget('CouponCode');
        $data = $request->all();

        if (empty(Auth::user()->email)){
            $data['user_email']='';
        }else{
            $data['user_email']= Auth::user()->email;
        }
        $session_id = Session::get('session_id');
        if (!isset($session_id)){
            $session_id =str_random(40);
            Session::put('session_id',$session_id);
        }
        $sizeIDArr = explode('-',$data['size']);
        $product_size = $sizeIDArr[1];
//        echo "<pre>"; print_r($data);
        $countProducts = DB::table('cart')->where([
            'product_id'=>$data['product_id'],
            'product_color'=>$data['product_color'],
            'size'=>$product_size,
            'session_id'=>$session_id
        ])->count();

        if ($countProducts>0){
            return redirect()->back()->with('error', 'Product already exist in Cart!');
        }else{
            $getsku = ProductAttribute::select('sku')->where(['product_id'=>$data['product_id'],'size'=>$product_size])->first();
            DB::table('cart')->insert([
                'product_id'=>$data['product_id'],
                'product_name'=>$data['product_name'],
                'product_code'=>$getsku->sku,
                'product_color'=>$data['product_color'],
                'price'=>$data['price'],
                'size'=>$product_size,
                'quantity'=>$data['quantity'],
                'user_email'=>$data['user_email'],
                'session_id'=>$session_id
            ]);
        }
        return redirect('cart')->with('success', 'Product has been added in Cart Successfully');
    }

    public function cart(){
        if (Auth::check()){
            $user_email = Auth::user()->email;
            $userCart = DB::table('cart')->where(['user_email'=>$user_email])->get();
        }else{
            $session_id = Session::get('session_id');
            $userCart = DB::table('cart')->where(['session_id'=>$session_id])->get();
        }
        foreach ($userCart as $key=>$product) {
            $products =Product::where('id',$product->product_id)->first();
            $userCart[$key]->image = $products ->image;
        }
//        echo "<pre>"; print_r($userCart);
        return view('frontend.cart',compact('userCart'));
    }
    public function deleteCart($id=null){
        Session::forget('CouponAmount');
        Session::forget('CouponCode');
        DB::table('cart')->where('id',$id)->delete();
        return redirect('cart')->with('success','Product has been deleted form Cart!');
    }
   public function updateCartQuantity($id=null ,$quantity=null){
       Session::forget('CouponAmount');
       Session::forget('CouponCode');
        $getCartDetails = DB::table('cart')->where('id',$id)->first();
        $getAttributeStock = ProductAttribute::where('sku',$getCartDetails->product_code)->first();
        $updateQuantity = $getCartDetails->quantity+$quantity;
        if ($getAttributeStock->stock >= $updateQuantity){
            DB::table('cart')->where('id',$id)->increment('quantity',$quantity);
            return redirect('cart')->with('success','Product Quantity has been updated successfully!');
        }else{
            return redirect('cart')->with('error','Required Product Quantity is not available!');
        }

   }
   public function applyCoupon(Request $request){
       Session::forget('CouponAmount');
       Session::forget('CouponCode');
        $data = $request->all();
        $coupons = Coupon::where('coupon_code', $data['coupon_code'])->count();
        if ($coupons==0){
            return redirect('cart')->with('error','Opps!! Product Coupon  does not exist!');
        }else{
            //get coupon details
            $coupons = Coupon::where('coupon_code', $data['coupon_code'])->first();
            //if coupon is inactive
            if ($coupons->status==0){
                return redirect('cart')->with('error','Opps!! Product Coupon  is not active!');

            }
            //if coupon is out of expiryDate
            $expiry_date =$coupons->expiry_date;
            $current_date = date('Y-m-d');
            if ($expiry_date<$current_date){
                return redirect('cart')->with('error','Opps!! Product Coupon  is expired!');
            }
            //coupon id valid for Discount
//            $session_id = Session::get('session_id');
            if (Auth::check()){
                $user_email = Auth::user()->email;
                $userCart = DB::table('cart')->where(['user_email'=>$user_email])->get();
            }else{
                $session_id = Session::get('session_id');
                $userCart = DB::table('cart')->where(['session_id'=>$session_id])->get();
            }
            $total_amount = 0;
            foreach ($userCart as $item) {
               $total_amount = $total_amount+($item->price*$item->quantity);
            }
            //check amount fixed or percentage
            if ($coupons->amount_type == "Fixed"){
                $couponAmount = $coupons->amount;
            }else{
                $couponAmount = $total_amount *($coupons->amount/100);
            }
            Session::put('CouponAmount',$couponAmount);
            Session::put('CouponCode',$data['coupon_code']);
            return redirect('cart')->with('success','Coupon Code successfully applied. You are availing discount!');
        }

   }
   public function checkout(Request $request){
        $user_id = Auth::user()->id;
        $user_email = Auth::user()->email;
        $user_details = User::find($user_id);
       $countries = Country::get();

       $shippingCount =Deliveryaddress::where('user_id', $user_id)->count();
       $shippingDetails = array();
       if ($shippingCount>0){
           $shippingDetails =Deliveryaddress::where('user_id',$user_id)->first();
       }

       $session_id = Session::get('session_id');
       DB::table('cart')->where(['session_id'=>$session_id])->update(['user_email'=>$user_email]);
       if ($request->isMethod('post')){
           $data = $request->all();

           User::where('id', $user_id)->update([
               'name'=> $data['billname'],
               'address1'=> $data['billaddress1'],
               'address2'=> $data['billaddress2'],
               'city'=> $data['billcity'],
               'state'=> $data['billstate'],
               'country'=> $data['billcountry'],
               'pincode'=> $data['billpincode'],
               'mobile'=> $data['billmobile']
           ]);

           if ($shippingCount>0){
               Deliveryaddress::where('user_id', $user_id)->update([
                   'name'=> $data['shipname'],
                   'address1'=> $data['shipaddress1'],
                   'address2'=> $data['shipaddress2'],
                   'city'=> $data['shipcity'],
                   'state'=> $data['shipstate'],
                   'country'=> $data['shipcountry'],
                   'pincode'=> $data['shippincode'],
                   'mobile'=> $data['shipmobile']
               ]);
           }else{
               $shipping = new Deliveryaddress ;
               $shipping->user_id = $user_id;
               $shipping->user_email = $user_email;
               $shipping->name = $data['shipname'];
               $shipping->address1 = $data['shipaddress1'];
               $shipping->address2 = $data['shipaddress2'];
               $shipping->city = $data['shipcity'];
               $shipping->pincode = $data['shippincode'];
               $shipping->country = $data['shipcountry'];
               $shipping->mobile = $data['shipmobile'];
               $shipping->save();
           }
           return redirect()->action('ProductController@orderReview');
       }
        return view('frontend.checkout',compact('user_details','countries','shippingDetails'));
   }
   public function orderReview(){
       $user_id = Auth::user()->id;
       $user_email = Auth::user()->email;
       $user_details = User::where('id',$user_id)->first();
       $shippingDetails =Deliveryaddress::where('user_id',$user_id)->first();
       $userCart = DB::table('cart')->where(['user_email'=>$user_email])->get();
       foreach ($userCart as $key=>$product) {
           $products =Product::where('id',$product->product_id)->first();
           $userCart[$key]->image = $products ->image;
       }
       return view('backend.products.orderReview',compact('user_details','shippingDetails','userCart'));
   }

   public function placeOrder(Request $request){
        if($request->isMethod('post')){
            $data =$request->all();
            $user_id = Auth::user()->id;
            $user_email = Auth::user()->email;
            $shippingDetails= Deliveryaddress::where(['user_email'=>$user_email])->first();
//            $shipping =json_decode(json_encode($shipping));

            if (empty(Session::get('CouponCode'))){
                $coupon_code = '';
            }else{
                $coupon_code =Session::get('CouponCode');
            }
            if (empty(Session::get('CouponAmount'))){
                $coupon_amount = '';
            }else{
                $coupon_amount =Session::get('CouponAmount');
            }


            $order = New Order;
            $order->user_id = $user_id;
            $order->user_email = $user_email;
            $order->name = $shippingDetails->name;
            $order->address = $shippingDetails->address;
            $order->city = $shippingDetails->city;
            $order->state = $shippingDetails->state;
            $order->pincode = $shippingDetails->pincode;
            $order->country = $shippingDetails->country;
            $order->mobile = $shippingDetails->mobile;
            $order->coupon_code = $coupon_code;
            $order->coupon_amount = $coupon_amount;
            $order->order_status ="New";
            $order->payment_method = $data['payment_method'];
            $order->grand_total = $data['grand_total'];
            $order->save();

            $order_id = DB::getPdo()->lastInsertId();
            $cartProducts = DB::table('cart')->where(['user_email'=>$user_email])->get();
            foreach ($cartProducts as $pro){
                $cartPro = new OrdersProduct;
                $cartPro->order_id = $order_id;
                $cartPro->user_id = $user_id;
                $cartPro->product_id = $pro->product_id;
                $cartPro->product_code = $pro->product_code;
                $cartPro->product_name = $pro->product_name;
                $cartPro->product_color = $pro->product_color;
                $cartPro->product_size = $pro->size;
                $cartPro->product_price = $pro->price;
                $cartPro->product_quantity = $pro->quantity;
                $cartPro->save();
            }
            Session::put('order_id', $order_id);
            Session::put('grand_total',$data['grand_total']);

            if ($data['payment_method']=="COD"){
                return redirect('/thanks');
            }else{
                return redirect('/paypal');
            }


        }

   }
   public function thanks(Request $request){
       $user_email = Auth::user()->email;
       DB::table('cart')->where('user_email', $user_email)->delete();
        return view('backend.products.thanks');
   }
   public function paypal(Request $request){
       $user_email = Auth::user()->email;
       DB::table('cart')->where('user_email', $user_email)->delete();
        return view('backend.products.paypal');
   }
   public function userOrder(){
        $user_id= Auth::user()->id;
        $orders = Order::with('orders')->where('user_id', $user_id)->latest()->get();
        return view('frontend.order.user_oder', compact('orders'));
   }
   public function userOrderDetails($order_id){
       $user_id= Auth::user()->id;
       $orderDetails = Order::with('orders')->where('id', $order_id)->first();
       return view('frontend.order.order_details', compact('orderDetails'));
   }
   public function viewOrder(){
        $orders = Order::with('orders')->latest()->get();
//        $orders = json_decode(json_encode($orders));
//        echo "<pre>"; print_r($orders);
        return view('backend.orders.viewOrder', compact('orders'));
   }
   public function viewOrderDetails($order_id){
       $orderDetails = Order::with('orders')->where('id', $order_id)->first();
       $orderDetails = json_decode(json_encode($orderDetails));
//       echo "<pre>" ; print_r($orderDetails); die;
       $user_id = $orderDetails->user_id;
       $userDetails = User::where('id',$user_id)->first();
       return view('backend.orders.viewOrderDetails' ,compact('orderDetails','userDetails'));
   }
   public function updateOrderStatus(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
//            echo "<pre>"; print_r($data);
            Order::where('id', $data['order_id'])->update(['order_status'=>$data['order_status']]);
        }
        return redirect()->back()->with('success', 'Order Status has benn updated Successfully');

   }

}
