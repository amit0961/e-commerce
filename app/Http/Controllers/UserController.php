<?php

namespace App\Http\Controllers;

use App\Country;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    public function userLoginRegister(){
        return view('users.login_register');
    }
    public function login(Request$request){
//        Session::forget('CouponAmount');
//        Session::forget('CouponCode');
        if($request->isMethod('post')){
            $data = $request->all();
            /*echo "<pre>"; print_r($data); die;*/
            if(Auth::attempt(['email'=>$data['email'],'password'=>$data['password']])){
                $userStatus = User::where('email',$data['email'])->first();
                if($userStatus->status == 0){
                    return redirect()->back()->with('error','Your account is not activated! Please confirm your email to activate.');
                }
                Session::put('frontSession',$data['email']);
                if(!empty(Session::get('session_id'))){
                    $session_id = Session::get('session_id');
                    DB::table('cart')->where('session_id',$session_id)->update(['user_email' => $data['email']]);
                }
                return redirect('/cart');
            }else{
                return redirect()->back()->with('error','Invalid Username or Password!');
            }
        }
    }

    public function register(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
            /*echo "<pre>"; print_r($data); die;*/
            // Check if User already exists
            $usersCount = User::where('email',$data['email'])->count();
            if($usersCount>0){
                return redirect()->back()->with('error','Email already exists!');
            }else{
                $user = new User;
                $user->name = $data['name'];
                $user->email = $data['email'];
                $user->password = bcrypt($data['password']);
                $user->save();
                /*// Send Register Email
                $email = $data['email'];
                $messageData = ['email'=>$data['email'],'name'=>$data['name']];
                Mail::send('emails.register',$messageData,function($message) use($email){
                    $message->to($email)->subject('Registration with E-com Website');
                });*/
                // Send Confirmation Email
                $email = $data['email'];
                $messageData = ['email'=>$data['email'],'name'=>$data['name'],'code'=>base64_encode($data['email'])];
                Mail::send('emails.confirmation',$messageData,function($message) use($email){
                    $message->to($email)->subject('Confirm your E-com Account');
                });
                return redirect()->back()->with('success','Please confirm your email to activate your account!');
                if(Auth::attempt(['email'=>$data['email'],'password'=>$data['password']])){
                    Session::put('frontSession',$data['email']);
                    if(!empty(Session::get('session_id'))){
                        $session_id = Session::get('session_id');
                        DB::table('cart')->where('session_id',$session_id)->update(['user_email' => $data['email']]);
                    }
                    return redirect('/cart');
                }
            }
        }
    }


    public function confirmAccount($email){
        $email = base64_decode($email);
        $userCount = User::where('email',$email)->count();
        if($userCount > 0){
            $userDetails = User::where('email',$email)->first();
            if($userDetails->status == 1){
                return redirect('login-register')->with('success','Your Email account is already activated. You can login now.');
            }else{
                User::where('email',$email)->update(['status'=>1]);
                // Send Welcome Email
                $messageData = ['email'=>$email,'name'=>$userDetails->name];
                Mail::send('emails.welcome',$messageData,function($message) use($email){
                    $message->to($email)->subject('Welcome to E-com Website');
                });
                return redirect('login-register')->with('success','Your Email account is activated. You can login now.');
            }
        }else{
            abort(404);
        }
    }

    public function logout(){
        Auth::logout();
        Session::forget('frontSession');
        Session::forget('session_id');
        return redirect('/');
    }

    public function checkEmail(Request$request){
        $data = $request->all();
        $userCount = User::where('email',$data['email'])->count();
        if ($userCount>0){
            echo "false";
        }else{
            echo "true";
        }
    }

    public function account(Request$request){
        $user_id =Auth::user()->id;
        $user_details = User::find($user_id);
        $countries = Country::get();

        if ($request->isMethod('post')){
            $data = $request->all();
//            echo "<pre>"; print_r($data); die;
            $user =User::find($user_id);
            $user->name = $data['name'];
            $user->address1 = $data['address1'];
            $user->address2 = $data['address2'];

            $user->city = $data['city'];
            $user->state = $data['state'];
            $user->country = $data['country'];
            $user->pincode = $data['pincode'];
            $user->mobile = $data['mobile'];
            $user->save();
            return redirect()->back()->with('success', 'Your account details has been updated successfully');
        }
        return view('users.account',compact('countries','user_details'));
    }
    public function checkuserPassword(Request$request){
        $data = $request->all();
        $current_password = $data['current_password'];
        $user_id = Auth::user()->id;
        $check_password = User::where('id',$user_id)->first();
        if (Hash::check($current_password, $check_password->password)){
            echo "true";  die;
        }else{
            echo "false"; die;
        }
    }

    public function updateuserPassword(Request$request){
        if ($request->isMethod('post')){
            $data = $request->all();
            $old_password = User::where('id' , Auth::User()->id)->first();
            $current_password = $data['current_password'];
            if (Hash::check($current_password,$old_password->password)){
                $new_password = bcrypt($data['new_password']);
                User::where('id',Auth::User()->id)->update(['password'=>$new_password]);
                return redirect()->back()->with('suc', 'Password updated successfully..!');
            }else{
                return redirect()->back()->with('err', 'Opps!  Current password is Incorrect..');
            }
        }
    }



}
