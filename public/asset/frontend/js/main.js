if ($.fn.slider) {
    $('#sl2').slider();
}

var RGBChange = function () {
    $('#RGB').css('background', 'rgb(' + r.getValue() + ',' + g.getValue() + ',' + b.getValue() + ')')
};

/*scroll to top*/

$(document).ready(function () {
    $(function () {
        $.scrollUp({
            scrollName: 'scrollUp', // Element ID
            scrollDistance: 300, // Distance from top/bottom before showing element (px)
            scrollFrom: 'top', // 'top' or 'bottom'
            scrollSpeed: 300, // Speed back to top (ms)
            easingType: 'linear', // Scroll to top easing (see http://easings.net/)
            animation: 'fade', // Fade, slide, none
            animationSpeed: 200, // Animation in speed (ms)
            scrollTrigger: false, // Set a custom triggering element. Can be an HTML string or jQuery object
            //scrollTarget: false, // Set a custom target element for scrolling to the top
            scrollText: '<i class="fa fa-angle-up"></i>', // Text for element, can contain HTML
            scrollTitle: false, // Set a custom <a> title if required.
            scrollImg: false, // Set true to use image
            activeOverlay: false, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
            zIndex: 2147483647 // Z-Index for the overlay
        });
    });
});
//change price & stock with size
$(document).ready(function () {
    $("#selSize").change(function () {
        var idSize = $(this).val();
        if(idSize == ""){
            return false;
        }
        $.ajax({
            type :'get',
            url:'/get-product-price',
            data:{idSize:idSize},
            success:function (res) {
                // alert(res);
                var arr= res.split('#');
                $("#getPrice").html("৳ "+arr[0]);
                $("#price").val(arr[0]);
                if (arr[1]==0){
                    $("#cartButton").hide();
                    $("#Availability").text("Out of Stock");
                }else{
                    $("#cartButton").show();
                    $("#Availability").text("In Stock");
                }
            },error:function () {
                alert("Error");
            }
        });
    });
});
//register form validate
$().ready(function () {
    $("#registerForm").validate( {
        rules:{
            name:{
                required:true,
                minlength:4,
            },
            email:{
                required: true,
                email: true,
                remote: "/check-email"
            },
            password:{
                required:true,
                minlength:6
            }
        },
        messages:{
            name: {
                required: "Please enter your name...",
                minlength:"Must be at least 4 characters long..."

            },
            password: {
                required:"Please provide your password...",
                minlength:"Must be at least 6 characters long..."
            },
            email:{
                required:"Please enter your email...",
                email:"Please enter your valid email...",
                remote: "Email already exists"
            }
        }
    });
    $("#accountForm").validate( {
        rules:{
            name:{
                required:true,
                minlength:4,
            },
            address:{
                required:true,
            },
            city:{
                required:true,
            },
            state:{
                required:true,
            },
            country:{
                required:true,
            },
            pincode:{
                required:true,
            },
            mobile:{
                required: true,
                minlength:11
            },

        },
        messages:{
            name: {
                required: "Please enter your name...",
                minlength:"Must be at least 4 characters long..."
            },
            address: {
                required: "Please enter your address...",
            },
            city: {
                required: "Please enter your city ...",
            },
            state: {
                required: "Please enter your state...",
            },
            country: {
                required: "Please select your country...",
            },
            pincode: {
                required: "Please provide your pincode...",
            },
            mobile: {
                required: "Please provide your phone no...",
                minlength:"Must be at least 11 characters long..."
            },
        }
    });

    $("#loginForm").validate({
        rules: {
            email: {
                required: true,
                email: true,
            },
            password: {
                required: true
            }
        },
        messages: {
            password: {
                required: "Please provide your password...",
            },
            email: {
                required: "Please enter your email...",
                email: "Please enter your valid email...",
            }
        }
    });

    $("#current_password").keyup(function () {
        var current_password = $(this).val();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="token"]').attr('content')
            },
            type:'get',
            url: '/check-userPswd',
            data:{current_password:current_password},
            success:function (resp) {
                if (resp=="false"){
                    $("#checkPassword").html("<font color='red'>Current Password is Incorrect..!</font>");
                }else if (resp=="true"){
                    $("#checkPassword").html("<font color='green'>Current Password is Correct..!</font>");
                }
            },error:function () {
                alert("Error");
            }
        });
        
    });




    $("#passwordForm").validate({
        rules: {
            current_password: {
                required: true,
                minlength:6,
                maxlength:15
            },
            new_password: {
                required: true,
                minlength:6,
                maxlength:15
            },
            confirm_password:{
                required: true,
                minlength:6,
                maxlength:15,
                equalTo:"#new_password"
            }
        },
        errorClass: "help-inline",
        errorElement: "span",
        highlight:function(element, errorClass, validClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
            $(element).parents('.control-group').addClass('success');
        }
    });

    $('#myPassword').passtrength({
        minChars: 4,
        passwordToggle: true,
        tooltip: true,
        eyeImg : "asset/frontend/images/eye.svg"
    });
    //copy billshipping

    $('#billToShip').click(function () {
        if (this.checked){
            $("#shipname").val($("#billname").val());
            $("#shipaddress1").val($("#billaddress1").val());
            $("#shipaddress2").val($("#billaddress2").val());
            $("#shipcity").val($("#billcity").val());
            $("#shipstate").val($("#billstate").val());
            $("#shipcountry").val($("#billcountry").val());
            $("#shippincode").val($("#billpincode").val());
            $("#shipmobile").val($("#billmobile").val());
        }else{
            $("#shipname").val('');
            $("#shipaddress1").val('');
            $("#shipaddress2").val('');
            $("#shipcity").val('');
            $("#shipstate").val('');
            $("#shipcountry").val('');
            $("#shippincode").val('');
            $("#shipmobile").val('');
        }
        
    })

    
});
//changeImage
$(document).ready(function () {
    $(".changeImage").click(function () {
        var image =$(this).attr('src');
        $(".mainImage").attr("src",image);
    });
});
// Instantiate EasyZoom instances
var $easyzoom = $('.easyzoom').easyZoom();

// Setup thumbnails example
var api1 = $easyzoom.filter('.easyzoom--with-thumbnails').data('easyZoom');

$('.thumbnails').on('click', 'a', function(e) {
    var $this = $(this);

    e.preventDefault();

    // Use EasyZoom's `swap` method
    api1.swap($this.data('standard'), $this.attr('href'));
});

// Setup toggles example
var api2 = $easyzoom.filter('.easyzoom--with-toggle').data('easyZoom');

$('.toggle').on('click', function() {
    var $this = $(this);

    if ($this.data("active") === true) {
        $this.text("Switch on").data("active", false);
        api2.teardown();
    } else {
        $this.text("Switch off").data("active", true);
        api2._init();
    }
});

function selectPaymentMethod() {
    if($('#Paypal').is(':checked') || $('#COD').is(':checked')){
        // alert("checked");
    }else{
        alert("Please select payment method first..!");
        return false;
    }

}
