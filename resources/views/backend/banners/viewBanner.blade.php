@extends('layouts.admin.adminDesign')
@section('content')
    <div id="content">
        <div id="content-header">
            <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Banners</a> <a href="#">View-Banner</a> </div>
            <h1>Banners</h1>
            @if (Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{!! session('success') !!}</strong>
                </div>
            @endif
        </div>
        <div class="container-fluid">
            <hr>
            <div class="row-fluid ">
                <div class="span12 ">
                    <div class="widget-box">
                        <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
                            <h5>View Banners</h5>
                        </div>
                        <div class="widget-content nopadding">
                            <table class="table table-bordered data-table ">
                                <thead>
                                <tr>
                                    <th>Banner ID</th>
                                    <th>Banner Title</th>
                                    <th>Banner Link</th>
                                    <th>Banner Status</th>
                                    <th>Image</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($banners as $banner)
                                    <tr class="gradeX " >
                                        <td>{{$banner->id}}</td>
                                        <td>{{$banner->title}}</td>
                                        <td>{{$banner->link}}</td>
                                        <td>
                                            @if($banner->status==1) Active @else  Inactive @endif
                                        </td>
                                        <td>
                                            @if(!empty($banner->image))
                                                <img src="{{ asset('images/products/banner/'.$banner->image) }}" style="width: 150px">
                                            @endif
                                        </td>
                                        <td class="center">
                                            <a href="{{url('admin/edit-banner/'. $banner->id)}}" class="btn btn-primary btn-mini" title="Banner Edit">Edit</a>
                                            <a href="{{url('admin/delete-banner/'. $banner->id)}}" id="deleteCat" class="btn btn-danger btn-mini">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
