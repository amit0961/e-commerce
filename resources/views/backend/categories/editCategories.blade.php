@extends('layouts.admin.adminDesign')
@section('content')
    <div id="content">
        <div id="content-header">
            <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Categories</a> <a href="#">Edit-Category</a> </div>
            <h1>Categories</h1>
        </div>
        <div class="container-fluid"><hr>
            <div class="row-fluid ">
                <div class="row-fluid">
                    <div class="span12" >
                        <div class="widget-box">
                            <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                                <h5>Edit-Category</h5>
                            </div>
                            <div class="widget-content nopadding">
                                <form class="form-horizontal " method="post" action="{{url('admin/edit-category/'.$categories->id)}}" name="add_category" id="add_category" novalidate="novalidate" >
                                    {{csrf_field()}}
                                    <div class="control-group">
                                        <label class="control-label">Category Name</label>
                                        <div class="controls">
                                            <input type="text" name="category_name" id="category_name" value="{{$categories->name}}" required />
                                            {{--                                            <span id="chkPwd"></span>--}}
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Category Level</label>
                                        <div class="controls">
                                            <select name="parent_id" id="" style="width: 220px;">
                                                <option value="0">Main Category</option>
                                                @foreach($levels as $level)
                                                    <option value="{{$level->id}}"
                                                            @if($level->id==$categories->parent_id)
                                                                selected
                                                            @endif
                                                    >{{$level->name}}</option>
                                                @endforeach

                                            </select>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Description</label>
                                        <div class="controls">
                                            <textarea  type="text" name="description" id="description"  required>{{$categories->description}} </textarea>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Url</label>
                                        <div class="controls">
                                            <input type="text" name="url" id="url" value="{{$categories->url}}" required />
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Enable</label>
                                        <div class="controls">
                                            <input type="checkbox" name="status" id="status" @if($categories->status=="1") checked @endif value="1" />
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <input type="submit" value="Add-Category" class="btn btn-success">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
