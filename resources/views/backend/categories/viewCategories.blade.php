@extends('layouts.admin.adminDesign')
@section('content')
    <div id="content">
        <div id="content-header">
            <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Categories</a> <a href="#">View-Category</a> </div>
            <h1>Categories</h1>
            @if (Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{!! session('success') !!}</strong>
                </div>
            @endif
        </div>
        <div class="container-fluid">
            <hr>
            <div class="row-fluid ">
                <div class="span12 ">
                    <div class="widget-box">
                        <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
                            <h5>View Categories</h5>
                        </div>
                        <div class="widget-content nopadding">
                            <table class="table table-bordered data-table ">
                                <thead>
                                <tr>
                                    <th>Category ID</th>
                                    <th>Category Name</th>
                                    <th>Sub Category</th>
                                    <th>URL</th>
                                    <th>Action</th>
                                </tr>
                                </thead>


                                <tbody>
{{--                                @if(is_array($categories) || is_object($categories))--}}
                                @foreach($categories as $category)
                                <tr class="gradeX " >
                                    <td>{{$category->id}}</td>
                                    <td>{{$category->name}}</td>
{{--                                    @foreach($category->sub_category as $sub_category)--}}
{{--                                        {!! $sub_category->name !!}--}}
{{--                                    @endforeach--}}
                                    <td>{{$category->parent_id}}</td>
                                    <td>{{$category->url}}</td>
                                    <td class="center">
                                        <a href="{{url('admin/edit-category/'. $category->id)}}" class="btn btn-primary btn-mini">Edit</a>
                                        <a href="{{url('admin/delete-category/'. $category->id)}}" id="deleteCat" class="btn btn-danger btn-mini">Delete</a></td>
                                </tr>
                                    @endforeach

                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
