@extends('layouts.admin.adminDesign')
@section('content')
    <div id="content">
        <div id="content-header">
            <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Coupons</a> <a href="#">View-Coupon</a> </div>
            <h1>Coupons</h1>
            @if (Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{!! session('success') !!}</strong>
                </div>
            @endif
        </div>
        <div class="container-fluid">
            <hr>
            <div class="row-fluid ">
                <div class="span12 ">
                    <div class="widget-box">
                        <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
                            <h5>View Coupons</h5>
                        </div>
                        <div class="widget-content nopadding">
                                <table class="table table-bordered data-table ">
                                    <thead>
                                    <tr>
                                        <th>Coupon ID</th>
                                        <th>Coupon Code</th>
                                        <th>Amount </th>
                                        <th>Amount Type</th>
                                        <th>Expiry Date</th>
                                        <th>Created Date</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>


                                    <tbody>
                                    {{--                                @if(is_array($Coupons) || is_object($Coupons))--}}
                                    @foreach($coupons as $coupon)
                                        <tr class="gradeX " >
                                            <td>{{$coupon->id}}</td>
                                            <td>{{$coupon->coupon_code}}</td>
                                            <td>{{$coupon->amount}}
                                                @if($coupon->amount_type=="Percentage") % @else  ৳ @endif
                                            </td>
                                            <td>{{$coupon->amount_type}}</td>
                                            <td>{{$coupon->expiry_date}}</td>
                                            <td>{{$coupon->created_at}}</td>
                                            <td>
                                                @if($coupon->status==1) Active @else  Inactive @endif
                                            </td>
                                            <td class="center">
                                                <a href="{{url('admin/edit-coupon/'. $coupon->id)}}" class="btn btn-primary btn-mini" title="Coupon Edit">Edit</a>
                                                <a href="{{url('admin/delete-coupon/'. $coupon->id)}}" id="deleteCat" class="btn btn-danger btn-mini">Delete</a>
                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
