@extends('layouts.admin.adminDesign')
@section('content')
    <!--main-container-part-->
    <div id="content">
        <div id="content-header">
            <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">Order Details</a> </div>
            <h1>View Order Details</h1>
        </div>
        @if (Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{!! session('success') !!}</strong>
            </div>
        @endif
        <div class="container-fluid">
            <hr>
            <div class="row-fluid">
                <div class="span6">
                    <div class="widget-box">
                        <div class="widget-title"> <span class="icon"><i class="icon-time"></i></span>
                            <h5>Order Details</h5>
                    </div>
                        <div class="widget-content nopadding">
                            <table class="table table-striped table-bordered">
                                <tbody>
                                <tr>
                                    <td class="taskDesc"><i class="icon-info-sign"></i>Order Date</td>
                                    <td class="taskStatus"><span class="in-progress">{{$orderDetails->created_at}}</span></td>
                                </tr>
                                <tr>
                                    <td class="taskDesc"><i class="icon-plus-sign"></i> Order Status</td>
                                    <td class="taskStatus"><span class="pending">{{$orderDetails->order_status}}</span></td>
                                </tr>
                                <tr>
                                    <td class="taskDesc"><i class="icon-plus-sign"></i> Order Total</td>
                                    <td class="taskStatus"><span class="pending">৳ {{$orderDetails->grand_total}}</span></td>
                                </tr>
                                <tr>
                                    <td class="taskDesc"><i class="icon-plus-sign"></i> Shipping Charges</td>
                                    <td class="taskStatus"><span class="pending">৳ {{$orderDetails->shipping_charges}}</span></td>
                                </tr>
                                <tr>
                                    <td class="taskDesc"><i class="icon-plus-sign"></i> Coupon Code</td>
                                    <td class="taskStatus"><span class="pending">{{$orderDetails->coupon_code}}</span></td>
                                </tr>
                                <tr>
                                    <td class="taskDesc"><i class="icon-plus-sign"></i> Coupon Amount</td>
                                    <td class="taskStatus"><span class="pending">৳ {{$orderDetails->coupon_amount}}</span></td>
                                </tr>
                                <tr>
                                    <td class="taskDesc"><i class="icon-plus-sign"></i>Payment Method</td>
                                    <td class="taskStatus"><span class="pending">{{$orderDetails->payment_method}}</span></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="span6">
                    <div class="widget-box">
                        <div class="widget-title"> <span class="icon"><i class="icon-time"></i></span>
                            <h5>Customer Details</h5>
                        </div>
                        <div class="widget-content nopadding">
                            <table class="table table-striped table-bordered">
                                <thead>
                                <tbody>
                                <tr>
                                    <td class="taskDesc"><i class="icon-info-sign"></i>Customer Name</td>
                                    <td class="taskStatus"><span class="in-progress">{{$orderDetails->name}}</span></td>
                                </tr>
                                <tr>
                                    <td class="taskDesc"><i class="icon-plus-sign"></i> Customer Email</td>
                                    <td class="taskStatus"><span class="pending">{{$orderDetails->user_email}}</span></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="widget-box">
                        <div class="widget-title"> <span class="icon"><i class="icon-time"></i></span>
                            <h5>Update Order Status</h5>
                        </div>
                        <div class="widget-content nopadding">
                            <form action="{{url('admin/update-order-status')}}" method="post">
                                {{csrf_field()}}
                                <input type="hidden" name="order_id" value="{{$orderDetails->id}}">
                                <table class="table table-striped ">
                                    <tr>
                                        <td>
                                            <select name="order_status" id="order_status" class="control-label" required>
                                                <option value="New" @if($orderDetails->order_status== "New") selected @endif >New</option>
                                                <option value="Pending" @if($orderDetails->order_status== "Pending") selected @endif >Pending</option>
                                                <option value="Cancelled" @if($orderDetails->order_status== "Cancelled") selected @endif >Cancelled</option>
                                                <option value="In Process" @if($orderDetails->order_status== "In Process") selected @endif >In Process</option>
                                                <option value="Shipped" @if($orderDetails->order_status== "Shipped") selected @endif >Shipped</option>
                                                <option value="Delivered" @if($orderDetails->order_status== "Delivered") selected @endif >Delivered</option>
                                            </select>
                                        </td>
                                        <td>
                                            <input class="btn btn-mini btn-success" type="submit" value="Update-Status">
                                        </td>
                                    </tr>
                                </table>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row-fluid">
                <div class="span6">
                    <div class="widget-box">
                        <div class="widget-title"> <span class="icon"> <i class="icon-eye-open"></i> </span>
                            <h5>Billing Address</h5>
                        </div>
                        <div class="widget-content nopadding">
                            <table class="table table-bordered">
                                <tbody>
                                <tr>
                                    <td>Name</td>
                                    <td>{{$userDetails->name}}</td>
                                </tr>
                                <tr>
                                    <td>Address</td>
                                    <td>{{$userDetails->address1}} {{$userDetails->address2}}</td>

                                </tr>
                                <tr>
                                    <td>City</td>
                                    <td>{{$userDetails->city}}</td>
                                </tr>
                                <tr>
                                    <td>State</td>
                                    <td>{{$userDetails->state}}</td>
                                </tr>
                                <tr>
                                    <td>Country</td>
                                    <td>{{$userDetails->country}}</td>
                                </tr>
                                <tr>
                                    <td>PinCode</td>
                                    <td>{{$userDetails->pincode}}</td>
                                </tr>
                                <tr>
                                    <td>Mobile</td>
                                    <td>{{$userDetails->mobile}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="span6">
                    <div class="widget-box">
                        <div class="widget-title"> <span class="icon"> <i class="icon-eye-open"></i> </span>
                            <h5>Shipping Address</h5>
                        </div>
                        <div class="widget-content nopadding">
                            <table class="table table-bordered">
                                <tbody>
                                <tr>
                                    <td>Name</td>
                                    <td>{{$orderDetails->name}}</td>
                                </tr>
                                <tr>
                                    <td>Address</td>
                                    <td>{{$orderDetails->address1}} {{$orderDetails->address2}}</td>

                                </tr>
                                <tr>
                                    <td>City</td>
                                    <td>{{$orderDetails->city}}</td>
                                </tr>
                                <tr>
                                    <td>State</td>
                                    <td>{{$orderDetails->state}}</td>
                                </tr>
                                <tr>
                                    <td>Country</td>
                                    <td>{{$orderDetails->country}}</td>
                                </tr>
                                <tr>
                                    <td>PinCode</td>
                                    <td>{{$orderDetails->pincode}}</td>
                                </tr>
                                <tr>
                                    <td>Mobile</td>
                                    <td>{{$orderDetails->mobile}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <hr>
            <div class="row-fluid">
                <div class="heading-center">
                    <table id="example" class="table table-striped table-bordered table-hover display" style="width:100%">
                        <thead>
                        <tr>
                            <th>Product Code</th>
                            <th>Product Name</th>
                            <th>Product Size</th>
                            <th>Product Color</th>
                            <th>Product Price</th>
                            <th>Product Quantity</th>
                        </thead>

                        <tbody>
                        @foreach($orderDetails->orders as $pro)
                            <tr>
                                <td>{{$pro->product_code}}</td>
                                <td>{{$pro->product_name}}</td>
                                <td>{{$pro->product_size}}</td>
                                <td>{{$pro->product_color}}</td>
                                <td>{{$pro->product_price}}</td>
                                <td>{{$pro->product_quantity}}</td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
    <!--main-container-part-->

@stop
