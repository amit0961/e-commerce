@extends('layouts.admin.adminDesign')
@section('content')
    <div id="content">
        <div id="content-header">
            <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Products</a> <a href="#">Add-Product Attributes</a> </div>
            <h1>Products Attributes</h1>
            @if (Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{!! session('success') !!}</strong>
                </div>
            @endif
            @if (Session::get('error'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{!! session('error') !!}</strong>
                </div>
            @endif
        </div>
        <div class="container-fluid"><hr>
            <div class="row-fluid ">
                <div class="row-fluid">
                    <div class="span12" >
                        <div class="widget-box">
                            <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                                <h5>Add-Product Attributes</h5>
                            </div>
                            <div class="widget-content nopadding">
                                <form enctype="multipart/form-data" class="form-horizontal " action="{{url('/admin/add-attribute/'.$products->id)}}" method="post"  name="addAttribute" id="addAttribute"  >
                                    {{csrf_field()}}
                                    <input type="hidden" name="product_id" value="{{$products->id}}">
                                    <div class="control-group">
                                        <label class="control-label">Product Name</label>
                                        <label class="control-label"><strong>{{$products->product_name}}</strong></label>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Product Code</label>
                                        <label class="control-label"><strong>{{$products->product_code}}</strong></label>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Product Color</label>
                                        <label class="control-label"><strong>{{$products->product_color}}</strong></label>
                                    </div>
                                    <div class="control-group">
                                        <div class="field_wrapper " id="add_attribute">
                                                <input  type="text" name="sku[]" id="sku" placeholder="SKU" value="" style="width: 120px" required/>
                                                <input  type="text" name="size[]" id="size" placeholder="Size" value="" style="width: 120px " required/>
                                                <input  type="text" name="price[]" id="price" placeholder="Price" value="" style="width: 120px" required/>
                                                <input  type="text" name="stock[]" id="stock" placeholder="Stock" value="" style="width: 120px" required/>
                                                <label class="control-label"> <a href="javascript:void(0);" class="add_button" title="Add field">ADD</a></label>
                                            <br>
                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <input type="submit" value="Add-Attribute" class="btn btn-success">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row-fluid ">
                <div class="span12 ">
                    <div class="widget-box">
                        <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
                            <h5>View Attributes</h5>
                        </div>
                        <div class="widget-content nopadding">
                            <form action="{{url('/admin/edit-attribute/'.$products->id)}}" method="post">
                                {{csrf_field()}}
                                <table class="table table-bordered data-table ">
                                    <thead>
                                        <tr>
                                            <th>Attribute ID</th>
                                            <th>SKU</th>
                                            <th>Size</th>
                                            <th>Price</th>
                                            <th>Stock</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    {{--                                @if(is_array($Products) || is_object($Products))--}}
                                        @foreach($products['productAttribute'] as $attribute)
                                            <tr class="gradeX " >
                                                <td><input type="hidden" name="idAttr[]" value="{{$attribute->id}}">{{$attribute->id}}</td>
                                                {{--                                        <td>{{$product->category_id}}</td>--}}
                                                <td>{{$attribute->sku}}</td>
                                                <td>{{$attribute->size}}</td>
                                                <td><input type="text" name="price[]" value="{{$attribute->price}}"></td>
                                                <td><input type="text" name="stock[]" value="{{$attribute->stock}}"></td>
                                                <td class="center">
                                                    <input type="submit" value="Update" class="btn btn-primary btn-mini">
                                                    <a href="{{url('/admin/delete-attribute/'. $attribute->id)}}" id="deleteCat" class="btn btn-danger btn-mini">Delete</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
