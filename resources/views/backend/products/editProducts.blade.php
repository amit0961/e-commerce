@extends('layouts.admin.adminDesign')
@section('content')
    <div id="content">
        <div id="content-header">
            <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Products</a> <a href="#">Edit-Product</a> </div>
            <h1>Products</h1>
            @if (Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{!! session('success') !!}</strong>
                </div>
            @endif
            @if (Session::get('error'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{!! session('error') !!}</strong>
                </div>
            @endif
        </div>
        <div class="container-fluid"><hr>
            <div class="row-fluid ">
                <div class="row-fluid">
                    <div class="span12" >
                        <div class="widget-box">
                            <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                                <h5>Edit-Product</h5>
                            </div>
                            <div class="widget-content nopadding">
                                <form enctype="multipart/form-data" class="form-horizontal " method="post" action="{{url('admin/edit-product/'.$products->id)}}" name="edit_product" id="edit_product" novalidate="novalidate" >
                                    {{csrf_field()}}
                                    <div class="control-group">
                                        <label class="control-label">Under Category </label>
                                        <div class="controls">
                                            <select name="category_id" id="" style="width: 220px;">
                                                <?php echo $categories_dropdown ; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Product Name</label>
                                        <div class="controls">
                                            <input type="text" name="product_name" id="product_name" value="{{$products->product_name}}" required />
                                            {{--                                            <span id="chkPwd"></span>--}}
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Product Code</label>
                                        <div class="controls">
                                            <input type="text" name="product_code" id="product_code" value="{{$products->product_code}}" required />
                                            {{--                                            <span id="chkPwd"></span>--}}
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Product Color</label>
                                        <div class="controls">
                                            <input type="text" name="product_color" id="product_color" value="{{$products->product_color}}" required />
                                            {{--                                            <span id="chkPwd"></span>--}}
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Description</label>
                                        <div class="controls">
                                            <textarea  type="text" name="description" id="description" required>{{$products->description}} </textarea>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Material & Care</label>
                                        <div class="controls">
                                            <textarea  type="text" name="care" id="care" required>{{$products->care}} </textarea>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Price</label>
                                        <div class="controls">
                                            <input type="text" name="price" id="price" value="{{$products->price}}" required />
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label">Image</label>
                                        <div class="controls">
                                            <input type="file" name="image" id="image"  >
                                            <input type="hidden" name="current_image" value="{{$products->image}}"  >
                                            @if(!empty($products->image))
                                            <img style="width: 50px" src="{{asset('/images/products/small/' .$products->image)}}" alt=" " > |
                                            <a  href="{{url('/admin/delete-product-image/'.$products->id)}}">Delete</a>
                                                @endif
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Enable</label>
                                        <div class="controls">
                                            <input type="checkbox" name="status" id="status" @if($products->status=="1") checked @endif value="1" />
                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <input type="submit" value="Edit-Product" class="btn btn-success">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
