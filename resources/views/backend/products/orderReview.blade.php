@extends('layouts.frontend.frontDesign')
@section('content')
    <section id="cart_items">
        <div class="container">
            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li class="active">Order Review</li>
                </ol>
            </div><!--/breadcrums-->
            <h1 class="text-center">Double Check Your Order Details</h1>
            <hr>
            <br>
            <br>
            <div class="col-md-6 order-md-1">
                    <h2 class="mb-3">Billing address</h2>
                    <br>
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label for="firstName">Full Name: {{$user_details->name}}</label>
                        </div>
                    </div>
                    <br>
                    <div class="mb-3">
                        <label for="address">Address 1: {{$user_details->address1}}</label>
                    </div>
                    <br>
                    <div class="mb-3">
                        <label for="address2">Address 2
                            <span class="text-muted">{{$user_details->address2}}</span>
                        </label>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3 mb-3">
                            <label for="zip">City: {{$user_details->city}} </label>
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for="zip">State: {{$user_details->state}}</label>
                        </div>
                        <div class="col-md-5 mb-3">
                            <label for="country">Country: {{$user_details->country}} </label>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3 mb-3">
                            <label for="firstName">Pincode: {{$user_details->pincode}}</label>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="firstName"> Contact: {{$user_details->mobile}} </label>
                        </div>
                    </div>
                    <br>
                </div>
            <div class="col-md-6 order-md-1">
                    <h2 class="mb-3">Shipping address</h2>
                    <br>
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label for="firstName">Full Name: {{$shippingDetails->name}} </label>
                        </div>
                    </div>
                    <br>
                    <div class="row">

                    </div>
                    <div class=" mb-3">
                        <label for="address">Address 1: {{$shippingDetails->address1}}</label>
                    </div>
                    <br>
                    <div class=" mb-3">
                        <label for="address2">Address 2:
                            <span class="text-muted">{{$shippingDetails->address2}}</span>
                        </label>
                    </div>
                    <br>

                    <div class="row">
                        <div class="col-md-3 mb-3">
                            <label for="zip">City: {{$shippingDetails->city}}</label>
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for="zip">State: {{$shippingDetails->state}}</label>
                        </div>
                        <div class="col-md-5 mb-3">
                            <label for="country">Country: {{$shippingDetails->country}}</label>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3 mb-3">
                            <label for="firstName">Pincode: {{$shippingDetails->pincode}}</label>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="firstName"> Contact: {{$shippingDetails->mobile}}</label>
                        </div>
                    </div>
                </div>
        </div>
    </section>
    <section id="cart_items">
        <div class="container">

    <div class="review-payment">
        <h2>Review & Payment</h2>
    </div>

    <div class="table-responsive cart_info">
        <table class="table table-condensed">
            <thead>
            <tr class="cart_menu">
                <td class="image">Item</td>
                <td class="description"></td>
                <td class="price">Price</td>
                <td class="quantity">Quantity</td>
                <td class="total">Total</td>
                <td></td>
            </tr>
            </thead>
            <tbody>
            <?php $totalAmount =0; ?>
            @foreach($userCart as $cart)
                <tr>
                    <td class="cart_product">
                        <a href=""><img style="height: 150px" src="{{asset('/images/products/small/'.$cart->image)}}" alt=""></a>
                    </td>
                    <td class="cart_description">
                        <h4><a href="">{{$cart->product_name}}</a></h4>
                        <p>CODE: {{$cart->product_code}} || {{$cart->size}}</p>
                    </td>
                    <td class="cart_price">
                        <p>৳ {{$cart->price}}</p>
                    </td>
                    <td class="cart_quantity">
                        <div class="cart_quantity_button">
                            <input class="cart_quantity_input" type="text" name="quantity" value="{{$cart->quantity}}" autocomplete="off" size="2">
                        </div>
                    </td>
                    <td class="cart_total">
                        <p class="cart_total_price">৳ {{$cart->price*$cart->quantity}}</p>
                    </td>
                </tr>
                <?php $totalAmount=$totalAmount+($cart->price*$cart->quantity); ?>
            @endforeach
            </tbody>
        </table>
    </div>
            <div class="row">
                <div class="col-md-6"></div>
                <div class="col-md-6 ">
                    <div class="total_area">
                        <ul>
                            @if(!empty(Session::get('CouponAmount')))
                                <li><label for="">Cart Sub Total</label> <span>৳ <?php echo $totalAmount; ?></span></li>
                                <li><label for="">Eco Tax </label><span>+ ৳ 200</span></li>
                                <li><label for=""> Shipping Cost </label><span>+ ৳ 100</span></li>
                                <li><label for="">* Coupon Discount  </label><span>- ৳ <?php echo Session::get('CouponAmount'); ?></span></li>
                                <li  class="animated bounce "> <strong><label for=""> You have to PAY---> </label><span  style="color:Red; ">= ৳ <?php echo $grandTotal=$totalAmount+200+100 - Session::get('CouponAmount'); ?> </span></strong></li>
                            @else
                                <li><label for="">Cart Sub Total</label> <span>৳ <?php echo $totalAmount; ?></span></li>
                                <li><label for="">Eco Tax </label><span>+ ৳ 200</span></li>
                                <li><label for=""> Shipping Cost </label><span>+ ৳ 100</span></li>
                                <li><label for=""> * Coupon Discount  </label><span>+ ৳ 0</span></li>
                                <li  class="animated bounce  "> <strong><label for=""> You have to PAY---> </label><span  style="color:Red; ">= ৳ <?php echo $grandTotal=$totalAmount+200+100; ?> </span> </strong> </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
            <hr class="mb-4">
            <form name="paymentForm" id="paymentForm" action="{{url('/place-order')}}" method="post">
                {{csrf_field()}}
                <input type="hidden" name="grand_total" value="{{$grandTotal}}">
                <div class="payment-options">
					<span>
                        <label><strong> Select Payment Method:</strong></label>
					</span>
                    <span>
						<label><input type="radio" name="payment_method" id="COD" value="Cash on Delivery"> <strong> Cash on Delivery</strong> </label>
					</span>
                    <span>
						<label><input type="radio" name="payment_method" id="Paypal" value="Paypal"> <strong>Paypal</strong></label>
					</span>
                    <span  style="float: right">
                        <button class="btn btn-default check_out" type="submit" onclick="return selectPaymentMethod();">
                        <i class="fa fa-truck"></i> Place Order</button>
                    </span>
                </div>
            </form>
        </div>
    </section> <!--/#cart_items-->
@stop
