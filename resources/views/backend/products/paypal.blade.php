@extends('layouts.frontend.frontDesign')
@section('content')
    <section id="cart_items">
        <div class="container">
            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li class="active">Order Review</li>
                </ol>
            </div><!--/breadcrums-->
            <div class="jumbotron text-center">
                <h1 class="display-3">Thank You!</h1>
                <p class="lead">Your order has been placed.</p>
                <hr>
                <p>
                    Your order number is = <strong>" {{Session::get('order_id')}} " </strong>and total payable about is = <strong> " ৳ {{Session::get('grand_total')}} " </strong>
                </p>
                <p>
                    Please make payment by clicking on below payment button
                </p>
                <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">
                    <input type="hidden" name="cmd" value="_xclick">
                    <input type="hidden" name="business" value="amitsahabusiness@gmail.com">
                    <input type="hidden" name="item_name" value="{{Session::get('order_id')}}">
                    <input type="hidden" name="amount" value="{{Session::get('grand_total')}}">
                    <input type="hidden" name="currency_code" value="INR">
{{--                    <input type="text" name="first_name" value="John">--}}
{{--                    <input type="text" name="last_name" value="Doe">--}}
{{--                    <input type="text" name="address1" value="9 Elm Street">--}}
{{--                    <input type="text" name="address2" value="Apt 5">--}}
{{--                    <input type="text" name="city" value="Berwyn">--}}
{{--                    <input type="text" name="state" value="PA">--}}
{{--                    <input type="text" name="zip" value="19312">--}}
                    <input type="image" name="submit"
                           src="https://www.paypalobjects.com/en_US/i/btn/btn_buynow_LG.gif"
                           alt="PayPal - The safer, easier way to pay online">
                    <img alt="" width="1" height="1"
                         src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" >
                </form>
            </div>
        </div>
    </section>
@stop
<?php
Session::forget('grand_total');
Session::forget('order_id');
?>
