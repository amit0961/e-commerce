@extends('layouts.frontend.frontDesign')
@section('content')
    <section id="cart_items">
        <div class="container">
            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li class="active">Order Review</li>
                </ol>
            </div><!--/breadcrums-->
            <div class="jumbotron text-center">
                <h1 class="display-3">Thank You!</h1>
                <p class="lead">Your<strong> Cash on Delivery </strong> order has been placed.</p>
                <hr>
                <p>
                    Your order number is = <strong>" {{Session::get('order_id')}} " </strong>and total payable about is = <strong> " ৳ {{Session::get('grand_total')}} " </strong>
                </p>
                <p class="lead">
                    <a class="btn btn-default check_out" href="{{url('/')}}" role="button">Continue to homepage</a>
                </p>
            </div>
        </div>
    </section>
@stop
<?php
    Session::forget('grand_total');
    Session::forget('order_id');
    ?>
