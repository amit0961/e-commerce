@extends('layouts.admin.adminDesign')
@section('content')
    <div id="content">
        <div id="content-header">
            <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Products</a> <a href="#">View-Product</a> </div>
            <h1>Products</h1>
            @if (Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{!! session('success') !!}</strong>
                </div>
            @endif
        </div>
        <div class="container-fluid">
            <hr>
            <div class="row-fluid ">
                <div class="span12 ">
                    <div class="widget-box">
                        <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
                            <h5>View Products</h5>
                        </div>
                        <div class="widget-content nopadding">
                            <table class="table table-bordered data-table ">
                                <thead>
                                <tr>
                                    <th>Product ID</th>
{{--                                    <th>Category ID</th>--}}
                                    <th>Category Name</th>
                                    <th>Product Name</th>
                                    <th>Product Code</th>
                                    <th>Product Color</th>
                                    <th>Price</th>
                                    <th>Image</th>
                                    <th>Action</th>
                                </tr>
                                </thead>


                                <tbody>
                                {{--                                @if(is_array($Products) || is_object($Products))--}}
                                @foreach($products as $product)
                                    <tr class="gradeX " >
                                        <td>{{$product->id}}</td>
{{--                                        <td>{{$product->category_id}}</td>--}}
                                        <td>{{$product->category_name}}</td>
                                        <td>{{$product->product_name}}</td>
                                        <td>{{$product->product_code}}</td>
                                        <td>{{$product->product_color}}</td>
                                        <td>{{$product->price}}</td>
                                        <td>
                                            @if(!empty($product->image))
                                            <img src="{{ asset('images/products/small/'.$product->image) }}" style="width: 50px">
                                                @endif
                                        </td>
                                        <td class="center">
                                            <a href="#myModal{{$product->id}}" data-toggle="modal" class="btn btn-success btn-mini" title="Product View">View</a>
                                            <a href="{{url('admin/edit-product/'. $product->id)}}" class="btn btn-primary btn-mini" title="Product Edit">Edit</a>
                                            <a href="{{url('admin/add-attribute/'. $product->id)}}" class="btn btn-warning btn-mini" title="Add Attribute">Attribute</a>
                                            <a href="{{url('admin/add-images/'. $product->id)}}" class="btn btn-info btn-mini" title="Add Images">Image</a>
                                            <a href="{{url('admin/delete-product/'. $product->id)}}" id="deleteCat" class="btn btn-danger btn-mini">Delete</a>
                                        </td>
                                    </tr>
                                    <div id="myModal{{$product->id}}" class="modal hide">
                                        <div class="modal-header">
                                            <button data-dismiss="modal" class="close" type="button">×</button>
                                            <h3>Full Details Here...</h3>
                                        </div>
                                        <div class="modal-body">
                                            <div >
                                                <p>Product Name = {{$product->product_name}} </p>
                                                <p>ID:-{{$product->id}}</p>
                                                <p>Category ID:-{{$product->category_id}}</p>
                                                <p>Code:-{{$product->product_code}}</p>
                                                <p>Color:{{$product->product_color}}</p>
                                                <p>Material & Care:{{$product->care}}</p>
                                                <p>Price: ৳ {{$product->price}}</p>
                                                <p>Description:{{$product->description}}</p>
                                            </div>
                                            <div >
                                                <img style="width: 150px ; " src="{{asset('/images/products/small/' .$product->image)}}" alt=" " >
                                            </div>
                                        </div>
                                    </div>
                                @endforeach

                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
