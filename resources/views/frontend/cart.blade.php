@extends('layouts.frontend.frontDesign')
@section('content')
    <section id="cart_items">
        <div class="container">
            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li class="active">Shopping Cart</li>
                </ol>
            </div>
            <div class="table-responsive cart_info">
                @if (Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{!! session('success') !!}</strong>
                    </div>
                @endif
                    @if (Session::get('error'))
                        <div class="alert alert-danger alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{!! session('error') !!}</strong>
                        </div>
                    @endif
                <table class="table table-condensed">
                    <thead>
                    <tr class="cart_menu">
                        <td class="image">Item</td>
                        <td class="description"></td>
                        <td class="price">Price</td>
                        <td class="quantity">Quantity</td>
                        <td class="total">Total</td>
                        <td></td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $totalAmount =0; ?>
                    @foreach($userCart as $cart)
                        <tr>
                        <td class="cart_product">
                            <a href=""><img style="height: 150px" src="{{asset('/images/products/small/'.$cart->image)}}" alt=""></a>
                        </td>
                        <td class="cart_description">
                            <h4><a href="">{{$cart->product_name}}</a></h4>
                            <p>CODE: {{$cart->product_code}} || {{$cart->size}}</p>
                        </td>
                        <td class="cart_price">
                            <p>৳ {{$cart->price}}</p>
                        </td>
                        <td class="cart_quantity">
                            <div class="cart_quantity_button">
                                <a class="cart_quantity_up" href="{{url('/cart/update-quantity/'.$cart->id.'/1')}}"> + </a>
                                    <input class="cart_quantity_input" type="text" name="quantity" value="{{$cart->quantity}}" autocomplete="off" size="2">
                                @if($cart->quantity>1)
                                    <a class="cart_quantity_down" href=""> - </a>
                                @endif
                            </div>
                        </td>
                        <td class="cart_total">
                            <p class="cart_total_price">৳ {{$cart->price*$cart->quantity}}</p>
                        </td>
                        <td class="cart_delete">
                            <a class="cart_quantity_delete" href="{{url('/cart/delete-product/'.$cart->id)}}"><i class="fa fa-times"></i></a>
                        </td>
                    </tr>
                        <?php $totalAmount=$totalAmount+($cart->price*$cart->quantity); ?>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </section> <!--/#cart_items-->

    <section id="do_action">
        <div class="container">
            <div class="heading">
                <h3>What would you like to do next?</h3>
                <p>Choose if you have a discount code or reward points you want to use.</p>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="chose_area">

                        <ul class="user_option">

                            <li>
                                <form action="{{url('cart/apply-coupon')}}" method="post">
                                    {{csrf_field()}}
                                <label><h4>* Use Coupon Code =</h4></label>
                                <input type="text"  name="coupon_code">
                                <input type="submit" value="Apply" class="btn btn-default" >
                                </form>
                            </li>

                        </ul>

                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="total_area">
                        <ul>
                            @if(!empty(Session::get('CouponAmount')))
                                <li>Cart Sub Total <span>৳ <?php echo $totalAmount; ?></span></li>
                                <li>Eco Tax <span>+ ৳ 200</span></li>
                                <li>Shipping Cost <span>+ ৳ 100</span></li>
                                <li>* Coupon Discount <span>- ৳ <?php echo Session::get('CouponAmount'); ?></span></li>
                                <li class="animated bounce "> <strong><label for=""> You have to PAY---> </label><span  style="color:#fe980f; ">= ৳ <?php echo $totalAmount+200+100 - Session::get('CouponAmount'); ?> </span></strong></li>
                            @else
                                <li>Cart Sub Total <span>৳ <?php echo $totalAmount; ?></span></li>
                                <li>Eco Tax <span>+ ৳ 200</span></li>
                                <li>Shipping Cost <span>+ ৳ 100</span></li>
                                <li class="animated bounce "> <strong><label for=""> You have to PAY---> </label><span  style="color:#fe980f; ">= ৳ <?php echo $totalAmount+200+100; ?> </span> </strong> </li>
                            @endif
                        </ul>
{{--                        @foreach($userCart as $cart)--}}
                        <div style="float: right">
                                <a class="btn btn-default update" href="">Update</a>
                                <a class="btn btn-default check_out" href="{{url('/checkout')}}">Check Out</a>
{{--                                @if($cart->quantity>1)--}}
{{--                                    <h4><span style="color: #fe980f;"><strong>Hello..! Please add any product to arrive Cart. </strong></span></h4>--}}
{{--                                @endif--}}
                        </div>
{{--                            @endforeach--}}


                    </div>
                </div>
            </div>
        </div>
    </section><!--/#do_action-->
@stop
