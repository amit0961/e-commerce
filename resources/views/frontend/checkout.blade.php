@extends('layouts.frontend.frontDesign')
@section('content')
    <section id="cart_items">
        <div class="container">
            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li class="active">Check out</li>
                </ol>
            </div><!--/breadcrums-->

            <form class="needs-validation" action="{{url('/checkout')}}" method="post">
                {{csrf_field()}}
                @if (Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{!! session('success') !!}</strong>
                    </div>
                @endif
                @if (Session::get('error'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{!! session('error') !!}</strong>
                    </div>
                @endif
                <div class="col-md-6 order-md-1">
                    <h2 class="mb-3">Billing address</h2>
                    <br>
                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <label for="firstName">Full Name</label>
                                <input id="billname" name="billname" value="{{$user_details->name}}" type="text" class="form-control"  placeholder="Full Name"  required>
                            </div>
                        </div>
                        <br>
                        <div class="mb-3">
                            <label for="address">Address 1</label>
                            <input id="billaddress1" name="billaddress1" type="text" value="{{$user_details->address1}}" type="text" class="form-control"  placeholder="Enter the address 1 here..." required>
                        </div>
                        <br>

                        <div class="mb-3">
                            <label for="address2">Address 2
                                <span class="text-muted">(Optional)</span>
                            </label>
                            <input id="billaddress2" name="billaddress2" type="text" value="{{$user_details->address2}}"  class="form-control"  placeholder="Apartment or suite">
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-md-3 mb-3">
                                <label for="zip">City</label>
                                <input id="billcity" name="billcity"  value="{{$user_details->city}}" type="text" class="form-control"  placeholder="" required>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="zip">State</label>
                                <input id="billstate" name="billstate" type="text" value="{{$user_details->state}}" class="form-control"  placeholder="" required>
                            </div>
                            <div class="col-md-5 mb-3">
                                <label for="country">Country</label>
                                <select name="billcountry" id="billcountry" class="custom-select d-block w-100"  required>
                                    <option value="">Select Country</option>
                                    @foreach($countries as $country)
                                        <option value="{{$country->country_name}}" @if($country->country_name == $user_details->country) selected @endif >{{$country->country_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3 mb-3">
                                <label for="firstName">Pincode</label>
                                <input id="billpincode" name="billpincode" type="text" value="{{$user_details->pincode}}" class="form-control"  placeholder="" value="" required>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="firstName"> Contact</label>
                                <input id="billmobile" name="billmobile" type="text" value="{{$user_details->mobile}}" class="form-control"  placeholder="" value="" required>
                            </div>
                        </div>
                        <br>
                        <div class="custom-control custom-checkbox">
                            <input value="{{$user_details->name}}" type="checkbox" id="billToShip" class="custom-control-input" >
                            <label class="custom-control-label" for="billToShip">Shipping address is the same as my billing address</label>
                        </div>
                </div>
                <div class="col-md-6 order-md-1">
                    <h2 class="mb-3">Shipping address</h2>
                    <br>
                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <label for="firstName">Full Name</label>
                                <input id="shipname" name="shipname" type="text" class="form-control"  placeholder="Full Name" value="{{$shippingDetails->name}}" required >
                            </div>
                        </div>
                        <br>
                        <div class="row">

                        </div>
                        <div class=" mb-3">
                            <label for="address">Address 1</label>
                            <input type="text" class="form-control" id="shipaddress1" name="shipaddress1" placeholder="Enter the address 1 here..." value="{{$shippingDetails->address1}}" required >
                        </div>
                        <br>
                        <div class=" mb-3">
                            <label for="address2">Address 2
                                <span class="text-muted">(Optional)</span>
                            </label>
                            <input type="text" class="form-control" id="shipaddress2" name="shipaddress2" placeholder="Apartment or suite" value="{{$shippingDetails->address2}}" required>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-md-3 mb-3">
                                <label for="zip">City</label>
                                <input type="text" class="form-control" id="shipcity" name="shipcity" placeholder="" value="{{$shippingDetails->city}}" required>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="zip">State</label>
                                <input type="text" class="form-control" id="shipstate" name="shipstate" placeholder="" value="{{$shippingDetails->state}}" required>
                            </div>
                            <div class="col-md-5 mb-3">
                                <label for="country">Country</label>
                                <select class="custom-select d-block w-100" name="shipcountry" id="shipcountry" value="{{$shippingDetails->country}}" required>
                                    <option value="">Select Country</option>
                                    @foreach($countries as $country)
                                        <option value="{{$country->country_name}}" @if($country->country_name == $shippingDetails->country) selected @endif >{{$country->country_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3 mb-3">
                                <label for="firstName">Pincode</label>
                                <input type="text" class="form-control" id="shippincode" name="shippincode" placeholder="" value="{{$shippingDetails->pincode}}" required>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="firstName"> Contact</label>
                                <input type="text" class="form-control" id="shipmobile" name="shipmobile" placeholder="" value="{{$shippingDetails->mobile}}" required>
                            </div>
                        </div>
                        <br>
                        <hr class="mb-4">
                        <button class="btn btn-default check_out" type="submit">
                            <i class="fa fa-credit-card"></i> Checkout</button>
                    <hr class="mt-4">
                    <br>

                </div>
            </form>
        </div>
    </section>


@stop
