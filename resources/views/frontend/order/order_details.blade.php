@extends('layouts.frontend.frontDesign')
@section('content')
    <section id="cart_items">
        <div class="container">
            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="{{url('/orders')}}"> Orders</a> </li>
                    <li class="active">ID: {{$orderDetails->id}}</li>
                </ol>
            </div><!--/breadcrums-->
            <div class="container">
                <div class="heading-center">
                    <table id="example" class="table table-striped table-bordered table-hover display" style="width:100%">
                        <thead>
                        <tr>
                            <th>Product Code</th>
                            <th>Product Name</th>
                            <th>Product Size</th>
                            <th>Product Color</th>
                            <th>Product Price</th>
                            <th>Product Quantity</th>
                        </thead>

                            <tbody>
                            @foreach($orderDetails->orders as $pro)
                            <tr>
                                <td>{{$pro->product_code}}</td>
                                <td>{{$pro->product_name}}</td>
                                <td>{{$pro->product_size}}</td>
                                <td>{{$pro->product_color}}</td>
                                <td>{{$pro->product_price}}</td>
                                <td>{{$pro->product_quantity}}</td>
                            </tr>
                            @endforeach
                            </tbody>

                    </table>
                </div>


            </div>
        </div>
    </section>
@stop
