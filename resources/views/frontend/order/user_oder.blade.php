@extends('layouts.frontend.frontDesign')
@section('content')
    <section id="cart_items">
        <div class="container">
            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li class="active">Order Review</li>
                </ol>
            </div><!--/breadcrums-->
            <div class="container">
                <div class="heading-center">
                    <table id="example" class="table table-striped table-bordered table-hover display" style="width:100%">
                        <thead>
                        <tr>
                            <th>Order ID</th>
                            <th>Ordered Products</th>
                            <th>Payment Method</th>
                            <th>Grand Total</th>
                            <th>Created On</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        @foreach($orders as $order)
                        <tbody>
                        <tr>
                            <td>{{$order->id}}</td>
                            <td>
                                @foreach($order->orders as $pro)
                                    <a href="{{url('/orders/'.$order->id)}}" >{{$pro->product_code}}</a>
                                     <br>
                                    @endforeach
                            </td>
                            <td>{{$order->payment_method}}</td>
                            <td>{{$order->grand_total}}</td>
                            <td>{{$order->created_at}}</td>
                            <td></td>
                        </tr>
                        </tbody>
                            @endforeach
                    </table>
                </div>


            </div>
        </div>
    </section>
@stop
