@extends('layouts.frontend.frontDesign')
@section('content')
    <section id="slider"><!--slider-->
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div id="slider-carousel" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
                            <li data-target="#slider-carousel" data-slide-to="1"></li>
                            <li data-target="#slider-carousel" data-slide-to="2"></li>
                        </ol>

                        <div class="carousel-inner">
                            <div class="item active">
                                <div class="col-sm-6">
                                    <h1><span>E</span>-SHOPPER</h1>
                                    <h2>Free E-Commerce Template</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                                    <button type="button" class="btn btn-default get">Get it now</button>
                                </div>
                                <div class="col-sm-6">
                                    <img src="{{asset('asset/frontend/images/home/girl1.jpg')}}" class="girl img-responsive" alt="" />
                                    <img src="{{asset('asset/frontend/images/home/pricing.png')}}"  class="pricing" alt="" />
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-sm-6">
                                    <h1><span>E</span>-SHOPPER</h1>
                                    <h2>100% Responsive Design</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                                    <button type="button" class="btn btn-default get">Get it now</button>
                                </div>
                                <div class="col-sm-6">
                                    <img src="{{asset('asset/frontend/images/home/girl2.jpg')}}" class="girl img-responsive" alt="" />
                                    <img src="{{asset('asset/frontend/images/home/pricing.png')}}"  class="pricing" alt="" />
                                </div>
                            </div>

                            <div class="item">
                                <div class="col-sm-6">
                                    <h1><span>E</span>-SHOPPER</h1>
                                    <h2>Free Ecommerce Template</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                                    <button type="button" class="btn btn-default get">Get it now</button>
                                </div>
                                <div class="col-sm-6">
                                    <img src="{{asset('asset/frontend/images/home/girl3.jpg')}}" class="girl img-responsive" alt="" />
                                    <img src="{{asset('asset/frontend/images/home/pricing.png')}}" class="pricing" alt="" />
                                </div>
                            </div>

                        </div>

                        <a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
                            <i class="fa fa-angle-left"></i>
                        </a>
                        <a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </div>

                </div>
            </div>
        </div>
    </section><!--/slider-->
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <div class="left-sidebar">
                        <h2>Category</h2>

                        <div class="panel-group category-products" id="accordian"><!--category-productsr-->
                            <div class="panel panel-default">
                                @foreach($categories as $category)
                                    @if($category->status==1)
                                        <div class="panel-heading">
                                            <h4 class="panel-title">

                                                <a data-toggle="collapse" data-parent="#accordian" href="#{{$category->id}}">
                                                    <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                                    {{$category->name}}
                                                </a>
                                            </h4>f
                                        </div>
                                    @endif
                                    <div id="{{$category->id}}" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <ul>
                                                @foreach($category->categories as $subcategory)
                                                    @if($subcategory->status==1)
                                                        <li><a href="{{asset('/products/'.$subcategory->url)}}">{{$subcategory->name}} </a></li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                @endforeach
                            </div>

                        </div><!--/category-products-->

                        <div class="price-range"><!--price-range-->
                            <h2>Price Range</h2>
                            <div class="well text-center">
                                <input type="text" class="span2" value="" data-slider-min="0" data-slider-max="600" data-slider-step="5" data-slider-value="[250,450]" id="sl2" ><br />
                                <b class="pull-left">$ 0</b> <b class="pull-right">$ 600</b>
                            </div>
                        </div><!--/price-range-->

                        <div class="shipping text-center"><!--shipping-->
                            <img src="{{asset('asset/frontend/images/home/shipping.jpg')}}" alt="" />
                        </div><!--/shipping-->

                    </div>
                </div>

                <div class="col-sm-9 padding-right">
                    <div class="product-details"><!--product-details-->
                        <div class="col-sm-5">
                            <div class="view-product">
                                <div class="easyzoom easyzoom--overlay easyzoom--with-thumbnails">
                                    <a href="{{asset('/images/products/large/'.$products->image)}}">
                                        <img style="width:300px;" class="mainImage" src="{{asset('/images/products/large/'.$products->image)}}" alt="" />
                                    </a>
                                </div>
                            </div>
                            <div id="similar-product" class="carousel slide" data-ride="carousel">

                                <!-- Wrapper for slides -->
                                <div class="carousel-inner">
                                    <div class="item active thumbnails">
                                        <a href="{{asset('/images/products/large/'.$products->image)}}" data-standard="{{asset('/images/products/small/'.$products->image)}}">
                                            <img class="changeImage" style="width:60px ; cursor:pointer;" src="{{asset('/images/products/small/'.$products->image)}}" alt="" />
                                        </a>
                                        @foreach($proAltImages as $altImage)
                                            <a href="{{asset('/images/products/large/'.$altImage->image)}}" data-standard="{{asset('/images/products/small/'.$altImage->image)}}">
                                                <img class="changeImage" style="width:60px ; cursor:pointer;"  src="{{asset('/images/products/small/'.$altImage->image)}}" >
                                            </a>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-7">
                            <form action="{{url('add-cart')}}" name="addtocartForm" id="addtocartForm" method="post">
                                {{csrf_field()}}
                                <input type="hidden" name="product_id" value="{{$products->id}}">
                                <input type="hidden" name="product_name" value="{{$products->product_name}}">
                                <input type="hidden" name="product_code" value="{{$products->product_code}}">
                                <input type="hidden" name="product_color" value="{{$products->product_color}}">
                                <input type="hidden" name="price" id="price" value="{{$products->price}}">
                                <div class="product-information"><!--/product-information-->
                                <img src="{{asset('/asset/frontend/images/product-details/new.jpg')}}" class="newarrival" alt="" />
                                <h2>{{ $products->product_name}}</h2>
                                <p>Product CODE: {{$products->product_code}}</p>
                                <p>Color: {{$products->product_color}}</p>
                                    @if (Session::get('error'))
                                        <div class="alert alert-danger alert-block">
                                            <button type="button" class="close" data-dismiss="alert">×</button>
                                            <strong>{!! session('error') !!}</strong>
                                        </div>
                                    @endif
                                <p>
                                    <select id="selSize" name="size" style="width: 150px" required>
                                        <option value="" disabled selected>Select Size</option>
                                        @foreach($products->productattribute as $sizes)
                                            <option value="{{$products->id}}-{{$sizes->size}}">{{$sizes->size}}</option>
                                        @endforeach
                                    </select>
                                </p>
{{--                                <img src="{{asset('/asset/frontend/images/product-details/rating.png')}}" alt="" />--}}
                                <span>
									<span id="getPrice">৳ {{$products->price}}</span>
									<label>Quantity:</label>
									<input type="text" name="quantity" value="1" />
                                    @if($totalStock>0)
                                        <button type="submit" class="btn btn-fefault cart" id="cartButton">
                                            <i class="fa fa-shopping-cart"></i>
                                            Add to cart
                                        </button>
                                    @endif
								</span>
                                <p>
                                    <b>Availability:</b>
                                    <span id="Availability">
                                        @if($totalStock>0)
                                            In Stock
                                        @else
                                            Out of Stock
                                        @endif
                                    </span>
                                </p>
                                <p><b>Condition:</b> New</p>
                                <a href=""><img src="images/product-details/share.png" class="share img-responsive"  alt="" /></a>
                            </div><!--/product-information-->
                            </form>
                        </div>
                    </div><!--/product-details-->

                    <div class="category-tab shop-details-tab"><!--category-tab-->
                        <div class="col-sm-12">
                            <ul class="nav nav-tabs">
                                <li ><a href="#description" data-toggle="tab">Description</a></li>
                                <li><a href="#care" data-toggle="tab">Material & Care</a></li>
                                <li class="active"><a href="#delivery" data-toggle="tab">Delivery Option</a></li>
{{--                                <li class="active"><a href="#reviews" data-toggle="tab">Reviews (5)</a></li>--}}
                            </ul>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane fade" id="description" >
                                <div class="col-sm-12 active in">
                                    <p>{{$products->description}}</p>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="care" >
                                <div class="col-sm-12">
                                    <p>{{$products->care}}</p>
                                </div>
                            </div>
                            <div class="tab-pane fade active in " id="delivery" >
                                <div class="col-sm-12">
                                    <p>here is the Delivery</p>
                                </div>
                            </div>
                        </div>
                    </div><!--/category-tab-->

                    <div class="recommended_items"><!--recommended_items-->
                        <h2 class="title text-center">Recommended & Related items</h2>

                        <div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <?php $count=1; ?>
                                    @foreach ($relatedProducts->chunk(3) as $chunk)
                                        <div <?php if ($count==1){?>  class="item active" <?php } else{ ?> class="item" <?php } ?> >
                                            @foreach ($chunk as $item)
                                                <div class="col-sm-4">
                                                    <div class="product-image-wrapper">
                                                        <div class="single-products">
                                                            <div class="productinfo text-center">
                                                                <img style="width: 150px" src="{{asset('/images/products/small/'.$item->image)}}" alt="" />
                                                                <h2>৳ {{$item->price}}</h2>
                                                                <p>{{ $item->product_name}}</p>
                                                                <a href="{{url('/product/'.$item->id)}}"><button type="button" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to Cart</button></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                    </div>
                                        <?php $count++; ?>
                                     @endforeach
                            </div>
                            <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
                                <i class="fa fa-angle-left"></i>
                            </a>
                            <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </div>
                    </div><!--/recommended_items-->

                </div>
            </div>
        </div>
    </section>
@stop
