<!DOCTYPE html>
<html lang="en">
<head>
    <title>Matrix Admin</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="{{asset('asset/backend/css/bootstrap.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('asset/backend/css/uniform.css')}}"/>
    <link rel="stylesheet" href="{{asset('asset/backend/css/bootstrap-responsive.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('asset/backend/css/fullcalendar.css')}}" />
    <link rel="stylesheet" href="{{asset('asset/backend/css/select2.css')}}" />
    <link rel="stylesheet" href="{{asset('asset/backend/css/matrix-style.css')}}" />
    <link rel="stylesheet" href="{{asset('asset/backend/css/matrix-media.css')}}" />
    <link href="{{asset('asset/backend/font-awesome/css/font-awesome.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset('asset/backend/css/jquery.gritter.css')}}" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
</head>
<body>

@include('layouts.admin.adminHeader')
@include('layouts.admin.adminSidebar')

@yield('content')
@include('layouts.admin.adminFooter')

{{--<script src="{{asset('asset/backend/js/excanvas.min.js')}}"></script>--}}
{{--<script src="{{asset('asset/backend/js/jquery.min.js')}}"></script>--}}
{{--<script src="{{asset('asset/backend/js/jquery.ui.custom.js')}}"></script>--}}
{{--<script src="{{asset('asset/backend/js/bootstrap.min.js')}}"></script>--}}
{{--<script src="{{asset('asset/backend/js/jquery.flot.min.js')}}"></script>--}}
{{--<script src="{{asset('asset/backend/js/jquery.flot.resize.min.js')}}"></script>--}}
{{--<script src="{{asset('asset/backend/js/jquery.peity.min.js')}}"></script>--}}
{{--<script src="{{asset('asset/backend/js/fullcalendar.min.js')}}"></script>--}}
{{--<script src="{{asset('asset/backend/js/matrix.js')}}"></script>--}}
{{--<script src="{{asset('asset/backend/js/matrix.dashboard.js')}}"></script>--}}
{{--<script src="{{asset('asset/backend/js/jquery.gritter.min.js')}}"></script>--}}
{{--<script src="{{asset('asset/backend/js/matrix.interface.js')}}"></script>--}}
{{--<script src="{{asset('asset/backend/js/matrix.chat.js')}}"></script>--}}
{{--<script src="{{asset('asset/backend/js/jquery.validate.js')}}"></script>--}}
{{--<script src="{{asset('asset/backend/js/matrix.form_validation.js')}}"></script>--}}
{{--<script src="{{asset('asset/backend/js/jquery.wizard.js')}}"></script>--}}
{{--<script src="{{asset('asset/backend/js/jquery.uniform.js')}}"></script>--}}
{{--<script src="{{asset('asset/backend/js/select2.min.js')}}"></script>--}}
{{--<script src="{{asset('asset/backend/js/matrix.popover.js')}}"></script>--}}
{{--<script src="{{asset('asset/backend/js/jquery.dataTables.min.js')}}"></script>--}}
{{--<script src="{{asset('asset/backend/js/matrix.tables.js')}}"></script>--}}

<script src="{{asset('asset/backend/js/jquery.min.js')}}"></script>
{{--<script src="{{asset('asset/backend/js/jquery.ui.custom.js')}}"></script>--}}
<script src="{{asset('asset/backend/js/bootstrap.min.js')}}"></script>
<script src="{{asset('asset/backend/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('asset/backend/js/jquery.uniform.js')}}"></script>
<script src="{{asset('asset/backend/js/select2.min.js')}}"></script>
<script src="{{asset('asset/backend/js/jquery.validate.js')}}"></script>
<script src="{{asset('asset/backend/js/matrix.js')}}"></script>
<script src="{{asset('asset/backend/js/matrix.tables.js')}}"></script>
<script src="{{asset('asset/backend/js/matrix.form_validation.js')}}"></script>
<script src="{{asset('asset/backend/js/matrix.popover.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $( function() {
        $( "#expiry_date" ).datepicker({
            minDate:0 ,
            dateFormat:'yy-mm-dd'
        });
    } );
</script>
</body>
</html>
