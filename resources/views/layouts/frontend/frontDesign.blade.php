<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Home | E-Shopper</title>
    <link href="{{asset('asset/frontend/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('asset/frontend/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('asset/frontend/css/prettyPhoto.css')}}" rel="stylesheet">
    <link href="{{asset('asset/frontend/css/price-range.css')}}" rel="stylesheet">
    <link href="{{asset('asset/frontend/css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('asset/frontend/css/main.css')}}" rel="stylesheet">
    <link href="{{asset('asset/frontend/css/main.css')}}" rel="stylesheet">
    <link href="{{asset('asset/frontend/css/passtrength.css')}}" rel="stylesheet">
    <link href="{{asset('asset/frontend/zoomApi/css/easyzoom.css')}}" rel="stylesheet">
    <script src="{{asset('asset/frontend/js/jquery.js')}}"></script>
    <!--[if lt IE 9]>
    <script src="{{asset('asset/frontend/js/html5shiv.js')}}"></script>
    <script src="{{asset('asset/frontend/js/respond.min.js')}}"></script>
    <![endif]-->
    <link rel="shortcut icon" href="{{asset('asset/frontend/images/ico/favicon.ico')}}">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{asset('asset/frontend/images/ico/apple-touch-icon-144-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{asset('asset/frontend/images/ico/apple-touch-icon-114-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{asset('asset/frontend/images/ico/apple-touch-icon-72-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" href="{{asset('asset/frontend/images/ico/apple-touch-icon-57-precomposed.png')}}">
</head><!--/head-->

<body>
@include('layouts.frontend.frontHeader')
{{--@include('layouts.frontend.frontSlide')--}}
@yield('content')
@include('layouts.frontend.frontFooter')





<script src="{{asset('asset/frontend/js/bootstrap.min.js')}}"></script>
<script src="{{asset('asset/frontend/js/jquery.scrollUp.min.js')}}"></script>
<script src="{{asset('asset/frontend/js/price-range.js')}}"></script>
<script src="{{asset('asset/frontend/js/jquery.prettyPhoto.js')}}"></script>
<script src="{{asset('asset/frontend/zoomApi/js/easyzoom.js')}}"></script>
<script src="{{asset('asset/frontend/js/main.js')}}"></script>
<script src="{{asset('asset/frontend/js/passtrength.js')}}"></script>
<script src="{{asset('asset/frontend/js/jquery.validate.js')}}"></script>

</body>
</html>
