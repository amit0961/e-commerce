@extends('layouts.frontend.frontDesign')
@section('content')
    <section id="form" style="margin-top: 20px;"><!--form-->
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-sm-offset-1">
                    <div class="login-form"><!--login form-->
                        @if (Session::get('success'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{!! session('success') !!}</strong>
                            </div>
                        @endif
                        @if (Session::get('error'))
                            <div class="alert alert-danger alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{!! session('error') !!}</strong>
                            </div>
                        @endif
                        <h2>Update account</h2>
                        <form id="accountForm" name="accountForm" method="POST" action="{{url('/account')}}">
                            {{csrf_field()}}
                            <input value="{{$user_details->name}}" id="name" name="name" type="text" placeholder="Name"  />
                            <input value="{{$user_details->address1}}" id="address" name="address" type="text" placeholder="Address line 1"  />
                            <input value="{{$user_details->address2}}" id="address" name="address" type="text" placeholder="Address line 2"  />
                            <input value="{{$user_details->city}}" id="city" name="city" type="text" placeholder="City"  />
                            <input value="{{$user_details->state}}" id="state" name="state" type="text" placeholder="State"  />
                            <select style="margin-bottom: 10px;" name="country" id="country">
                                <option value="">Select Country</option>
                                @foreach($countries as $country)
                                    <option value="{{$country->country_name}}"@if($country->country_name == $user_details->country) selected @endif>{{$country->country_name}}</option>
                                    @endforeach
                            </select>
                            <input value="{{$user_details->pincode}}" id="pincode" name="pincode" type="text" placeholder="Pin-Code"  />
                            <input value="{{$user_details->mobile}}" id="mobile" name="mobile" type="text" placeholder="Phone"  />
                            <button type="submit" class="btn btn-default">Signup</button>
                        </form>

                    </div><!--/login form-->
                </div>
                <div class="col-sm-1">
                    <h2 class="or">OR</h2>
                </div>
                <div class="col-sm-4">
                    <div class="signup-form"><!--sign up form-->
                        @if (Session::get('suc'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{!! session('suc') !!}</strong>
                            </div>
                        @endif
                        @if (Session::get('err'))
                            <div class="alert alert-danger alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{!! session('err') !!}</strong>
                            </div>
                        @endif
                        <h2>Update Password</h2>
                        <form id="passwordForm" name="passwordForm" method="POST" action="{{url('/update-userPswd')}}">
                            {{csrf_field()}}
                            <input  id="current_password" name="current_password" type="password" placeholder="Current Password"  />
                            <span id="checkPassword"></span>
                            <input value="" id="new_password" name="new_password" type="password" placeholder="New Password"  />
                            <input value="" id="confirm_password" name="confirm_password" type="password" placeholder="Confirm Password"  />
                            <button type="submit" class="btn btn-default">Update</button>
                        </form>
                    </div><!--/sign up form-->
                </div>
            </div>
        </div>
    </section><!--/form-->

@stop
