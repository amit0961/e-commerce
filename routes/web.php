<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'IndexController@index');


Route::match(['get', 'post'],'/admin/', 'AdminController@login' );


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Category List page
Route::get('/products/{url}', 'ProductController@products');
//productDetails Page
Route::get('/product/{id}', 'ProductController@productDetails');
//get product price
Route::get('/get-product-price','ProductController@getproductPrice');
//cart section
Route::match(['get', 'post'],'/add-cart', 'ProductController@addToCart' );
Route::match(['get', 'post'],'/cart', 'ProductController@cart' );
Route::get('/cart/delete-product/{id}', 'ProductController@deleteCart' );
Route::get('/cart/update-quantity/{id}/{quantity}','ProductController@updateCartQuantity');


//Coupon in Frontend\
Route::post('/cart/apply-coupon','ProductController@applyCoupon');



// Users loginRegister
Route::get( '/login-register', 'UserController@userLoginRegister' );
//register page from submit
Route::post('/user-register','UserController@register');
//confirm email
Route::get( '/confirm/{code}', 'UserController@confirmAccount');

Route::get('/user-logout','UserController@logout');
Route::post('/user-login','UserController@login');

//all routes after login
Route::group([ 'middleware' => ['frontlogin']], function()
{
    //userAccount Page
    Route::match(['get', 'post'], '/account', 'UserController@account' );
    //user account password check
    Route::get('/check-userPswd', 'UserController@checkuserPassword');
    Route::post('/update-userPswd', 'UserController@updateuserPassword');
    Route::match(['get', 'post'],'/checkout', 'ProductController@checkout');
    //order review page
    Route::match(['get', 'post'],'/order-review', 'ProductController@orderReview');
    Route::match(['get', 'post'],'/place-order', 'ProductController@placeOrder');
    Route::get('/thanks', 'ProductController@thanks');
    Route::get('/paypal', 'ProductController@paypal'); //>>>>>>>paypal method <<<<<<<<<<
    Route::get('/orders', 'ProductController@userOrder');
    Route::get('/orders/{id}', 'ProductController@userOrderDetails');


});



//Route::match(['GET', 'POST'], '/login-register', 'UserController@register' );
//mainJs EmailCheck
Route::match(['GET', 'POST'], '/check-email', 'UserController@checkEmail' );





Route::group([ 'middleware' => ['auth']], function()
{
    Route::get('/admin/dashboard', 'AdminController@dashboard');
    Route::get('/admin/settings', 'AdminController@settings');
    Route::match(['get', 'post'], '/admin/check-pwd', 'AdminController@checkPassword');
    Route::match(['get', 'post'], '/admin/update', 'AdminController@update' );

    //**********  For Categories  ********
    Route::match(['get', 'post'], 'admin/add-category', 'CategoryController@create' );
    Route::match(['get', 'post'], 'admin/edit-category/{id}', 'CategoryController@edit' );
    Route::match(['get', 'post'], 'admin/delete-category/{id}', 'CategoryController@delete' );
    Route::get('admin/view-category', 'CategoryController@show');

//**********  For Product  ********
    Route::match(['get', 'post'], 'admin/add-product', 'ProductController@create' );
    Route::match(['get', 'post'], 'admin/edit-product/{id}', 'ProductController@edit' );
    Route::get('admin/delete-product/{id}', 'ProductController@delete' );
    Route::get('admin/delete-product-image/{id}', 'ProductController@deleteImage' );
    Route::get('admin/view-product', 'ProductController@show');


//**********  For Product_Attribute  ********
    Route::match(['get', 'post'], 'admin/add-attribute/{id}', 'ProductController@addAttribute' );
    Route::match(['get', 'post'], 'admin/edit-attribute/{id}', 'ProductController@editAttribute' );
    Route::match(['get', 'post'], 'admin/add-images/{id}', 'ProductController@addImages' );
    Route::match(['get', 'post'], 'admin/delete-attribute/{id}', 'ProductController@deleteAttribute' );
    Route::match(['get', 'post'], 'admin/delete-image/{id}', 'ProductController@deleteProImage' );

//**********  For coupon section  ********
    Route::match(['get', 'post'],'admin/add-coupon', 'CouponController@addCoupon');
    Route::get('admin/view-coupon','CouponController@viewCoupons');
    Route::match(['get', 'post'], 'admin/edit-coupon/{id}', 'CouponController@editCoupon' );
    Route::get('admin/delete-coupon/{id}', 'CouponController@deleteCoupon' );
//**********  For Banner section  ********
    Route::match(['get', 'post'],'admin/add-banner', 'BannerController@addBanner');
    Route::get('admin/view-banner','BannerController@viewBanner');
    Route::match(['get', 'post'], 'admin/edit-banner/{id}', 'BannerController@editBanner' );
    Route::get('admin/delete-banner/{id}', 'BannerController@deleteBanner' );
//view order   *************
    Route::get('/admin/view-order', 'ProductController@viewOrder');
    Route::get('/admin/view-order/{id}', 'ProductController@viewOrderDetails');
    Route::post('/admin/update-order-status', 'ProductController@updateOrderStatus');







});
Route::get('/logout', 'AdminController@logout');
